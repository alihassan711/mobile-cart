package alburraq.mobilecart.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.widget.ImageView;

import java.util.ArrayList;

import alburraq.mobilecart.models.LoggedInUser;
import alburraq.mobilecart.models.MenuItems;
import alburraq.mobilecart.models.Product;
import alburraq.mobilecart.models.Shipping;
import alburraq.mobilecart.models.User;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class GlobalValues {


//    public static String BASE_URL="http://mobile-cart.al-burraq.com/api/";
    public static String BASE_URL="http://furniturepak.com/wp-content/plugins/mobile-cart-plugin/mobile.php";   //Wordpress
    public static ArrayList<Product> products=new ArrayList<>();
    public static ArrayList<Product> recommendedProducts=new ArrayList<>();
    public static ArrayList<MenuItems> menuItemsArrayList=new ArrayList<>();
    public static ArrayList<Shipping> shippingArrayList=new ArrayList<>();
    public static int selectedCategoryID =0;
    public static int selectedProductID =0;
    public static LoggedInUser user;

    public static boolean isListSet=false;



    public static void setUserLoginStatus(Context ctx, boolean isLogin){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("UserLoginStatus",isLogin);
        editor.commit();
    }

    public static boolean getUserLoginStatus(Context ctx){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        boolean isLogin = sharedPreferences.getBoolean("UserLoginStatus", false);
        return isLogin;
    }


    public static void backgroundBitmap(ImageView imageView, int id, Context context){
        Bitmap d= BitmapFactory.decodeResource(context.getResources(), id);
        int newHeight = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
        Bitmap putImage = Bitmap.createScaledBitmap(d, 512, newHeight, true);
        BitmapDrawable ob = new BitmapDrawable(context.getResources(), putImage);
        imageView.setBackgroundDrawable(ob);
    }


    public static void setTextToInputLayout(TextInputLayout textInputLayout, String text) {
        if (textInputLayout != null && textInputLayout.getEditText() != null) {
            textInputLayout.getEditText().setText(text);
        }
    }


    public static String getTextFromInputLayout(TextInputLayout textInputLayout) {
        if (textInputLayout != null && textInputLayout.getEditText() != null) {
            return textInputLayout.getEditText().getText().toString();
        } else {
            return null;
        }
    }


    public static boolean checkTextInputLayoutValueRequirement(TextInputLayout textInputLayout, String errorValue) {
        if (textInputLayout != null && textInputLayout.getEditText() != null) {
            String text = GlobalValues.getTextFromInputLayout(textInputLayout);
            if (text == null || text.isEmpty()) {
                textInputLayout.setErrorEnabled(true);
                textInputLayout.setError(errorValue);
                return false;
            } else {
                textInputLayout.setErrorEnabled(false);
                return true;
            }
        } else {
            return false;
        }
    }

    public static boolean isNetworkAvailable(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}

