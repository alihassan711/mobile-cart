package alburraq.mobilecart.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alburraq.mobilecart.R;
import alburraq.mobilecart.adapters.ExpandableListAdapter;
import alburraq.mobilecart.fragments.ListProducts;
import alburraq.mobilecart.fragments.ListProductsByCategory;
import alburraq.mobilecart.fragments.OrderProductsFragment;
import alburraq.mobilecart.models.MenuItems;
import alburraq.mobilecart.utils.GlobalValues;

import static alburraq.mobilecart.utils.GlobalValues.selectedCategoryID;

/**
 * Created by Al-Burraq Technologies on 4/4/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class LandingActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{

    Fragment fragment = null;
    private List<MenuItems> listDataHeader;
    private HashMap<Integer, List<MenuItems>> listDataChild;
    ExpandableListAdapter expandListAdapter;
    ExpandableListView expListView;
    DrawerLayout drawer;
    public static TextView badgeTv;
    ImageView headerCartImageView;
    public static EditText editText_filter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.landing_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        badgeTv=(TextView) findViewById(R.id.badge_textView);
        headerCartImageView=(ImageView) findViewById(R.id.header_cart_ImageView);
        editText_filter = (EditText)findViewById(R.id.editText_filter);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView =  navigationView.getHeaderView(0);

        if (savedInstanceState == null) {
            fragment = new ListProducts();
            FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }

        //Set Navigation Drawer data
        enableExpandableList();

        headerCartImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment=new OrderProductsFragment();
                String backStateName =  fragment.getClass().getName();
                String fragmentTag = backStateName;

                FragmentManager fragmentManager =getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(backStateName);
                fragmentTransaction.commit();
            }
        });
    }


    void navFragmentHandler(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            }
            else
            {
                super.onBackPressed();
            }
        }
    }


    private void enableExpandableList() {
        listDataHeader = new ArrayList<MenuItems>();
        listDataChild = new HashMap<Integer, List<MenuItems>>();

        // using expandable List View for category sub-category of items
        expListView = (ExpandableListView) findViewById(R.id.left_drawer);

        //adding navigation drawer data in header and child list
        prepareListData(listDataHeader, listDataChild);
        expandListAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(expandListAdapter);

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if(listDataChild.get(listDataHeader.get(groupPosition).getCATEGORY_ID()).size()==0) {

                    Toast.makeText(
                            getApplicationContext(),
                            listDataHeader.get(groupPosition).getCATEGORY_TITLE(), Toast.LENGTH_SHORT)
                            .show();
                    selectedCategoryID = 0;
                    fragment = new ListProducts();
                    navFragmentHandler();
                    drawer.closeDrawer(GravityCompat.START);
                }
                return false;
            }
        });

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
            }
        });

        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition).getCATEGORY_TITLE()
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition).getCATEGORY_ID()).get(
                                childPosition).getCATEGORY_ID(), Toast.LENGTH_SHORT)
                        .show();

                    selectedCategoryID =listDataChild.get(listDataHeader.get(groupPosition).getCATEGORY_ID()).get(childPosition).getCATEGORY_ID();
                    fragment = new ListProductsByCategory();
                    navFragmentHandler();
                    drawer.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    private void prepareListData(List<MenuItems> listDataHeader, Map<Integer, List<MenuItems>> listDataChild) {

//      Get Data from menu items arrayList and sort as categories and sub-categories

        for(int i=0;i< GlobalValues.menuItemsArrayList.size();i++){
            if(GlobalValues.menuItemsArrayList.get(i).getCATEGORY_PARENT_ID()== 0){
//                Adding header data
                listDataHeader.add(GlobalValues.menuItemsArrayList.get(i));
//                Adding child data
                List<MenuItems> menuItemsArrayList = new ArrayList<MenuItems>();
                for(int j=0;j<GlobalValues.menuItemsArrayList.size();j++){
                    if(GlobalValues.menuItemsArrayList.get(j).getCATEGORY_PARENT_ID()== GlobalValues.menuItemsArrayList.get(i).getCATEGORY_ID()) {
                        menuItemsArrayList.add(GlobalValues.menuItemsArrayList.get(j));
                    }
                }

                listDataChild.put(GlobalValues.menuItemsArrayList.get(i).getCATEGORY_ID(),menuItemsArrayList);
            }
        }
    }
}
