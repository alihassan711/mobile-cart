package alburraq.mobilecart.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import alburraq.mobilecart.R;
import alburraq.mobilecart.models.MenuItems;
import alburraq.mobilecart.utils.GlobalValues;
import alburraq.mobilecart.utils.JSONParser;

import static alburraq.mobilecart.utils.GlobalValues.backgroundBitmap;


/**
 * Created by Al-Burraq Technologies on 4/4/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class SplashActivity extends AppCompatActivity {

    Handler handler;
    Runnable runnable;
    boolean next = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView imageView=(ImageView)findViewById(R.id.bg);

        // Setting bitmap to avoid memory exceptions
        backgroundBitmap(imageView,R.drawable.splash_screen,this);
        if(GlobalValues.isNetworkAvailable(this)){
            getMenusFromServer();
        }else {
//			getMenu();
        }

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if(next){
                    Intent intent = new Intent(SplashActivity.this, LoginRegisterActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    next = true;
                    handler.postDelayed(runnable, 2000);
                }
            }
        };

        runnable.run();
    }


    public void getMenusFromServer() {

        GlobalValues.menuItemsArrayList=new ArrayList<>();

        new AsyncTask<String, Void, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected JSONObject doInBackground(String... strings) {
                JSONParser parser = new JSONParser();
                JSONObject JObject = parser.makeHttpRequest(GlobalValues.BASE_URL + "getMenus", "GET", null);
                return JObject;
            }

            @Override
            protected void onPostExecute(JSONObject JObject) {
                String status = null;
                if (JObject != null) {
                    try {
                        status = JObject.get("success").toString();
                        if(status.equals("0")){
                            String msg=JObject.get("message").toString();
                            Toast.makeText(SplashActivity.this,msg,Toast.LENGTH_LONG).show();
                            return;
                        }
                        else if(status.equals("1")){
                            if(JObject.has("menus")) {
                                JSONArray menuJsonArray = JObject.getJSONArray("menus");
                                for(int i=0;i<menuJsonArray.length();i++) {

                                    JSONObject jsonObj = menuJsonArray.getJSONObject(i);
                                    MenuItems menuItems = new MenuItems();

                                    menuItems.setCATEGORY_ID(jsonObj.getInt("categoryId"));
                                    menuItems.setCATEGORY_PARENT_ID(jsonObj.getInt("parentId"));
                                    menuItems.setCATEGORY_TITLE(jsonObj.getString("categoryName"));

                                    int priority=jsonObj.getInt("categoryPriority");
                                    if(priority==0) {
                                        menuItems.setCATEGORY_PRIORITY(false);
                                    }else{
                                        menuItems.setCATEGORY_PRIORITY(true);
                                    }

                                    // add categories and sub-categories data into menu items arrayList for later use
                                    GlobalValues.menuItemsArrayList.add(menuItems);

                                    Log.e("MenuItems",""+GlobalValues.menuItemsArrayList.size());
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

}
