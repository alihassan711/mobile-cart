package alburraq.mobilecart.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import alburraq.mobilecart.R;
import alburraq.mobilecart.fragments.LoginFragment;
import alburraq.mobilecart.fragments.RegisterFragment;
import io.fabric.sdk.android.Fabric;

import static alburraq.mobilecart.utils.GlobalValues.backgroundBitmap;


/**
 * Created by Al-Burraq Technologies on 4/4/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class LoginRegisterActivity extends AppCompatActivity {

    Button button_login, button_register;
    Fragment loginFragment, registerFragment;
    // Note: Your consumer key and secret should be changed in your source code before using.
    private static final String TWITTER_KEY = "J8hpVMzdMHKQNdgomK4wtSmgD";
    private static final String TWITTER_SECRET = "UeMjLdNg7sRIHJW0xcRrlNvhxqQOcleRpK4V8OKNRtMfycDK3v";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Twitter Config with Fabric
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        setContentView(R.layout.activity_login_register);

        ImageView imageView=(ImageView)findViewById(R.id.bg);
        // Setting bitmap to avoid memory exceptions
        backgroundBitmap(imageView,R.drawable.login_background,this);

        button_login = (Button) findViewById(R.id.imageView_login);
        button_register = (Button) findViewById(R.id.imageView_register);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginFragment = new LoginFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.fade_in_slowed, R.anim.fade_out);
                ft.replace(R.id.container, loginFragment, "LoginFragment").commit();
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerFragment = new RegisterFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.fade_in_slowed, R.anim.fade_out);
                ft.replace(R.id.container, registerFragment, "RegisterFragment").commit();
            }
        });

        if (savedInstanceState == null) {
            loginFragment = new LoginFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, loginFragment).commit();
        }
    }


    // Twitter response call onActivityResult of main activity first then it call onActivityResult inside fragment

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (loginFragment != null) {
            loginFragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
