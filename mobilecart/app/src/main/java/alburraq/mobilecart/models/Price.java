package alburraq.mobilecart.models;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class Price {
    int PRICE;// product price in shop currency (numeric) -->
    int PROMO_PRICE;//discount price of product in shop currency (numeric) -->

    public Price() {
    }

    public int getPRICE() {
        return PRICE;
    }

    public void setPRICE(int PRICE) {
        this.PRICE = PRICE;
    }

    public int getPROMO_PRICE() {
        return PROMO_PRICE;
    }

    public void setPROMO_PRICE(int PROMO_PRICE) {
        this.PROMO_PRICE = PROMO_PRICE;
    }
}
