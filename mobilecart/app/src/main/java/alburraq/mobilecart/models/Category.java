package alburraq.mobilecart.models;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class Category {
    String CATEGORYTEXT;
    int CATEGORYID;


    public Category() {
    }

    public String getCATEGORYTEXT() {
        return CATEGORYTEXT;
    }

    public void setCATEGORYTEXT(String CATEGORYTEXT) {
        this.CATEGORYTEXT = CATEGORYTEXT;
    }

    public int getCATEGORYID() {
        return CATEGORYID;
    }

    public void setCATEGORYID(int CATEGORYID) {
        this.CATEGORYID = CATEGORYID;
    }
}
