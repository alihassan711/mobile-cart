package alburraq.mobilecart.models;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class StockKeepingUnit {
    int ID;
    int PCS_IN_STOCK;// number of pieces in stock -->
    int COLOR_ID;// variant color id -->
    int SIZE_ID;// variant size id -->

    public StockKeepingUnit() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPCS_IN_STOCK() {
        return PCS_IN_STOCK;
    }

    public void setPCS_IN_STOCK(int PCS_IN_STOCK) {
        this.PCS_IN_STOCK = PCS_IN_STOCK;
    }

    public int getCOLOR_ID() {
        return COLOR_ID;
    }

    public void setCOLOR_ID(int COLOR_ID) {
        this.COLOR_ID = COLOR_ID;
    }

    public int getSIZE_ID() {
        return SIZE_ID;
    }

    public void setSIZE_ID(int SIZE_ID) {
        this.SIZE_ID = SIZE_ID;
    }
}
