package alburraq.mobilecart.models;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class MenuItems {


    int CATEGORY_ID;
    int CATEGORY_PARENT_ID;
    String CATEGORY_TITLE;
    boolean CATEGORY_PRIORITY;


    public MenuItems() {
    }


    public int getCATEGORY_ID() {
        return CATEGORY_ID;
    }

    public void setCATEGORY_ID(int CATEGORY_ID) {
        this.CATEGORY_ID = CATEGORY_ID;
    }

    public int getCATEGORY_PARENT_ID() {
        return CATEGORY_PARENT_ID;
    }

    public void setCATEGORY_PARENT_ID(int CATEGORY_PARENT_ID) {
        this.CATEGORY_PARENT_ID = CATEGORY_PARENT_ID;
    }

    public String getCATEGORY_TITLE() {
        return CATEGORY_TITLE;
    }

    public void setCATEGORY_TITLE(String CATEGORY_TITLE) {
        this.CATEGORY_TITLE = CATEGORY_TITLE;
    }

    public boolean isCATEGORY_PRIORITY() {
        return CATEGORY_PRIORITY;
    }

    public void setCATEGORY_PRIORITY(boolean CATEGORY_PRIORITY) {
        this.CATEGORY_PRIORITY = CATEGORY_PRIORITY;
    }
}
