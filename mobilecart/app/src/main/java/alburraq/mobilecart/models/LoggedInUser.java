package alburraq.mobilecart.models;

import org.json.JSONObject;

/**
 * Created by Al-Burraq-Dev on 5/24/2017.
 */

public class LoggedInUser {

    public int id,socialBool;
    public String firstName,lastName,email,image;


    public LoggedInUser(JSONObject user){
        try{
            id = user.getInt("id");
            firstName = user.getString("firstname");
            lastName = user.getString("lastname");
            email = user.getString("email");
            image = user.getString("image");
            socialBool = user.getInt("social"); // 1 for social ... 0 for native
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
