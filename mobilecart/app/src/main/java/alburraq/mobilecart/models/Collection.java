package alburraq.mobilecart.models;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

//collection of the product
public class Collection {
    String COLLECTIONTEXT;//
    int COLLECTION_ID;//collection id (integer)


    public Collection() {
    }

    public String getCOLLECTIONTEXT() {
        return COLLECTIONTEXT;
    }

    public void setCOLLECTIONTEXT(String COLLECTIONTEXT) {
        this.COLLECTIONTEXT = COLLECTIONTEXT;
    }

    public int getCOLLECTION_ID() {
        return COLLECTION_ID;
    }

    public void setCOLLECTION_ID(int COLLECTION_ID) {
        this.COLLECTION_ID = COLLECTION_ID;
    }
}
