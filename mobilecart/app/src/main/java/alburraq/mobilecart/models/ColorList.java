package alburraq.mobilecart.models;

import java.util.List;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class ColorList {
    List<ColorsImage> IMAGES;
    int ID;
    String NAME;
    String RGB;
    String CODE;

    public ColorList() {
    }

    public List<ColorsImage> getIMAGES() {
        return IMAGES;
    }

    public void setIMAGES(List<ColorsImage> IMAGES) {
        this.IMAGES = IMAGES;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getRGB() {
        return RGB;
    }

    public void setRGB(String RGB) {
        this.RGB = RGB;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }
}
