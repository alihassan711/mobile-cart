package alburraq.mobilecart.models;

import java.util.List;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class Product {
    String PRODUCT_NAME;
    int BEST_SALES_RANK;//            <!-- rank for sort order best sales (integer) -->
    int PRODUCT_ID;//original product id (integer)
    int ITEM_GROUP_ID;//variants group id
    boolean IS_NEW = false; //is product new? (bool)
    boolean SALE = false;//is product discounted (bool)
    Brand BRAND;
    String DESCRIPTION; //product description in HTML (string)
    String PRODUCT_URL;
    Price PRICE;
    List<Size> SIZES;
    List<ColorList> COLORS;
    List<Collection> COLLECTIONS;
    List<Category> CATEGORIES;
    List<StockKeepingUnit> SKUS;


    public Product() {
    }

    public String getPRODUCT_NAME() {
        return PRODUCT_NAME;
    }

    public void setPRODUCT_NAME(String PRODUCT_NAME) {
        this.PRODUCT_NAME = PRODUCT_NAME;
    }

    public int getBEST_SALES_RANK() {
        return BEST_SALES_RANK;
    }

    public void setBEST_SALES_RANK(int BEST_SALES_RANK) {
        this.BEST_SALES_RANK = BEST_SALES_RANK;
    }

    public int getPRODUCT_ID() {
        return PRODUCT_ID;
    }

    public void setPRODUCT_ID(int PRODUCT_ID) {
        this.PRODUCT_ID = PRODUCT_ID;
    }

    public int getITEM_GROUP_ID() {
        return ITEM_GROUP_ID;
    }

    public void setITEM_GROUP_ID(int ITEM_GROUP_ID) {
        this.ITEM_GROUP_ID = ITEM_GROUP_ID;
    }

    public boolean IS_NEW() {
        return IS_NEW;
    }

    public void setIS_NEW(boolean IS_NEW) {
        this.IS_NEW = IS_NEW;
    }

    public boolean isSALE() {
        return SALE;
    }

    public void setSALE(boolean SALE) {
        this.SALE = SALE;
    }

    public Brand getBRAND() {
        return BRAND;
    }

    public void setBRAND(Brand BRAND) {
        this.BRAND = BRAND;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getPRODUCT_URL() {
        return PRODUCT_URL;
    }

    public void setPRODUCT_URL(String PRODUCT_URL) {
        this.PRODUCT_URL = PRODUCT_URL;
    }

    public Price getPRICE() {
        return PRICE;
    }

    public void setPRICE(Price PRICE) {
        this.PRICE = PRICE;
    }

    public List<Size> getSIZES() {
        return SIZES;
    }

    public void setSIZES(List<Size> SIZES) {
        this.SIZES = SIZES;
    }

    public List<ColorList> getCOLORS() {
        return COLORS;
    }

    public void setCOLORS(List<ColorList> COLORS) {
        this.COLORS = COLORS;
    }

    public List<Collection> getCOLLECTIONS() {
        return COLLECTIONS;
    }

    public void setCOLLECTIONS(List<Collection> COLLECTIONS) {
        this.COLLECTIONS = COLLECTIONS;
    }

    public List<Category> getCATEGORIES() {
        return CATEGORIES;
    }

    public void setCATEGORIES(List<Category> CATEGORIES) {
        this.CATEGORIES = CATEGORIES;
    }

    public List<StockKeepingUnit> getSKUS() {
        return SKUS;
    }

    public void setSKUS(List<StockKeepingUnit> SKUS) {
        this.SKUS = SKUS;
    }
}
