package alburraq.mobilecart.models;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class Cart {

    int CART_ID;
    int CART_PRODUCT_ID;
    int CART_PRODUCT_PRICE;
    int CART_PRODUCT_QUANTITY;
    String CART_PRODUCT_COLOR;
    String CART_PRODUCT_SIZE;
    String CART_PRODUCT_NAME;
    String CART_PRODUCT_IMAGE;


    public Cart() {
    }


    public int getCART_ID() {
        return CART_ID;
    }

    public void setCART_ID(int CART_ID) {
        this.CART_ID = CART_ID;
    }

    public int getCART_PRODUCT_ID() {
        return CART_PRODUCT_ID;
    }

    public void setCART_PRODUCT_ID(int CART_PRODUCT_ID) {
        this.CART_PRODUCT_ID = CART_PRODUCT_ID;
    }

    public int getCART_PRODUCT_PRICE() {
        return CART_PRODUCT_PRICE;
    }

    public void setCART_PRODUCT_PRICE(int CART_PRODUCT_PRICE) {
        this.CART_PRODUCT_PRICE = CART_PRODUCT_PRICE;
    }

    public int getCART_PRODUCT_QUANTITY() {
        return CART_PRODUCT_QUANTITY;
    }

    public void setCART_PRODUCT_QUANTITY(int CART_PRODUCT_QUANTITY) {
        this.CART_PRODUCT_QUANTITY = CART_PRODUCT_QUANTITY;
    }

    public String getCART_PRODUCT_COLOR() {
        return CART_PRODUCT_COLOR;
    }

    public void setCART_PRODUCT_COLOR(String CART_PRODUCT_COLOR) {
        this.CART_PRODUCT_COLOR = CART_PRODUCT_COLOR;
    }

    public String getCART_PRODUCT_SIZE() {
        return CART_PRODUCT_SIZE;
    }

    public void setCART_PRODUCT_SIZE(String CART_PRODUCT_SIZE) {
        this.CART_PRODUCT_SIZE = CART_PRODUCT_SIZE;
    }

    public String getCART_PRODUCT_NAME() {
        return CART_PRODUCT_NAME;
    }

    public void setCART_PRODUCT_NAME(String CART_PRODUCT_NAME) {
        this.CART_PRODUCT_NAME = CART_PRODUCT_NAME;
    }

    public String getCART_PRODUCT_IMAGE() {
        return CART_PRODUCT_IMAGE;
    }

    public void setCART_PRODUCT_IMAGE(String CART_PRODUCT_IMAGE) {
        this.CART_PRODUCT_IMAGE = CART_PRODUCT_IMAGE;
    }
}
