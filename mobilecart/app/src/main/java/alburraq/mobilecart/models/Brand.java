package alburraq.mobilecart.models;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class Brand {
    int BRAND_ID;//brand id (integer)
    String BRAND_NAME;//
    String BRAND_IMG;//url of brand image (string)
    boolean IS_PREMIUM;//is brand premium? (bool)


    public Brand() {
    }


    public int getBRAND_ID() {
        return BRAND_ID;
    }

    public void setBRAND_ID(int BRAND_ID) {
        this.BRAND_ID = BRAND_ID;
    }

    public String getBRAND_NAME() {
        return BRAND_NAME;
    }

    public void setBRAND_NAME(String BRAND_NAME) {
        this.BRAND_NAME = BRAND_NAME;
    }

    public String getBRAND_IMG() {
        return BRAND_IMG;
    }

    public void setBRAND_IMG(String BRAND_IMG) {
        this.BRAND_IMG = BRAND_IMG;
    }

    public boolean IS_PREMIUM() {
        return IS_PREMIUM;
    }

    public void setIS_PREMIUM(boolean IS_PREMIUM) {
        this.IS_PREMIUM = IS_PREMIUM;
    }
}
