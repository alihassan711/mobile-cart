package alburraq.mobilecart.interfaces;
import alburraq.mobilecart.models.Shipping;


/**
 * Created by Al-Burraq Technologies on 4/16/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public interface ShippingDialogInterface {

    // Shipping dialog interface called on shipping selected
    void onShippingSelected(Shipping shipping);
}
