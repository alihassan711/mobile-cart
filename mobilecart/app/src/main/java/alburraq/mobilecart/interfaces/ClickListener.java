package alburraq.mobilecart.interfaces;

import android.view.View;
import java.util.ArrayList;
import alburraq.mobilecart.models.Product;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public interface ClickListener {

    //Product Item click interface
    public void itemClicked(View view, int position, ArrayList<Product> productArrayList);
}