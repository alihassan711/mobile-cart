package alburraq.mobilecart.interfaces;

import android.view.View;
import java.util.List;
import alburraq.mobilecart.models.Shipping;

/**
 * Created by Al-Burraq Technologies on 4/17/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public interface SelectedShippingListener {

    // Selected Shipping Method Click interface
    public void itemClicked(View view, int position, List<Shipping> shippingArrayList);
}
