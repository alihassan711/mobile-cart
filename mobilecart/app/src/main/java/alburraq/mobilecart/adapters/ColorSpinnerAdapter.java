package alburraq.mobilecart.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import alburraq.mobilecart.R;
import alburraq.mobilecart.models.ColorList;

/**
 * Created by Al-Burraq Technologies on 4/10/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class ColorSpinnerAdapter extends ArrayAdapter<ColorList> {
    private static final int layoutID = R.layout.spinner_item_product_color;
    private final LayoutInflater layoutInflater;

    private List<ColorList> productColorList;

    /**
     * Creates an adapter for color selection.
     */
    public ColorSpinnerAdapter(Context context) {
        super(context, layoutID);
        this.productColorList = new ArrayList<>();
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return productColorList.size();
    }

    public ColorList getItem(int position) {
        return productColorList.get(position);
    }

    public long getItemId(int position) {
        return productColorList.get(position).getID();
    }

    public void setProductColorList(List<ColorList> productColors) {

        //Set product possible color arrayList
        if (productColors != null) {
            this.productColorList.addAll(productColors);
            notifyDataSetChanged();
        }
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ListItemHolder holder;

        if (v == null) {
            v = layoutInflater.inflate(layoutID, parent, false);
            holder = new ListItemHolder();
            holder.colorImage = (RoundedImageView) v.findViewById(R.id.color_picker_image_view);
            holder.colorText = (TextView) v.findViewById(R.id.color_picker_text);
            holder.colorStroke = (LinearLayout) v.findViewById(R.id.color_picker_image_stroke);
            v.setTag(holder);
        } else {
            holder = (ListItemHolder) v.getTag();
        }

        // get color from list and displaying using Picasso
        ColorList color = productColorList.get(position);
        Picasso.with(getContext()).cancelRequest(holder.colorImage);
        if (color != null) {
            holder.colorText.setText(color.getNAME());

                if (color.getRGB() != null && (!color.getRGB().isEmpty())) {
                    final String hexColor = color.getRGB();
                    GradientDrawable gradDrawable = (GradientDrawable) holder.colorImage.getBackground();
                    int resultColor = 0xffffffff;
                    try {
                        resultColor = Color.parseColor(hexColor);
                    } catch (Exception e) {

                    }
                    gradDrawable.setColor(resultColor);
                }
        }
        return v;
    }

    static class ListItemHolder {
        TextView colorText;
        RoundedImageView colorImage;
        LinearLayout colorStroke;
    }
}