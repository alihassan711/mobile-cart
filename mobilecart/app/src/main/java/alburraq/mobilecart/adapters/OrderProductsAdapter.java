package alburraq.mobilecart.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

import alburraq.mobilecart.R;
import alburraq.mobilecart.db.MySQLiteHelper;
import alburraq.mobilecart.models.Cart;
import alburraq.mobilecart.models.Product;
import alburraq.mobilecart.utils.GlobalValues;

import static alburraq.mobilecart.activities.LandingActivity.badgeTv;

/**
 * Created by Al-Burraq Technologies on 4/12/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class OrderProductsAdapter extends RecyclerView.Adapter<OrderProductsAdapter.ViewHolder> {


    // Order product adapter where product editing, product deletion functionality are performed
    private ArrayList<Cart> cartArrayList;
    Context context;
    MySQLiteHelper db;
    Spinner editCartSizeSpinner;

    EditText editCartQuantityEditText;
    ImageButton editCartIncrementQuantityButton;
    ImageButton editCartDecrementQuantityButton;

    int selectedSize=0;
    int selectedQuantity=0;

    List<String> editCartSizeSpinnerList;
    int lastPosition = -1;

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView productName;
        public TextView productPrice;
        public TextView productCount;
        public TextView productSizeColor;
        public ImageView productImageView;
        public LinearLayout deleteItemLinearLayout;
        public LinearLayout editItemLinearLayout;
        public ViewHolder(View v) {
            super(v);
            productImageView = (ImageView) v.findViewById(R.id.shipping_item_imageView);
            productName = (TextView) v.findViewById(R.id.textView_shipping_item_name);
            productPrice = (TextView) v.findViewById(R.id.textView_shipping_item_price);
            productCount = (TextView) v.findViewById(R.id.shipping_per_item_count_textView);
            productSizeColor = (TextView) v.findViewById(R.id.shipping_item_size_color);
            deleteItemLinearLayout = (LinearLayout) v.findViewById(R.id.delete_item_LinearLayout);
            editItemLinearLayout = (LinearLayout) v.findViewById(R.id.edit_item_LinearLayout);
        }
    }

    public OrderProductsAdapter(ArrayList<Cart> cartArrayList, Context context) {
        this.cartArrayList=cartArrayList;
        this.context=context;
        db=new MySQLiteHelper(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_items_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.productName.setText(cartArrayList.get(position).getCART_PRODUCT_NAME());
        holder.productCount.setText(cartArrayList.get(position).getCART_PRODUCT_QUANTITY()+" pcs");
        holder.productSizeColor.setText(cartArrayList.get(position).getCART_PRODUCT_COLOR()+
                                        " / "+cartArrayList.get(position).getCART_PRODUCT_SIZE());
        int price =cartArrayList.get(position).getCART_PRODUCT_PRICE()*cartArrayList.get(position).getCART_PRODUCT_QUANTITY();
        holder.productPrice.setText("$"+price+".00");
        String temp = cartArrayList.get(position).getCART_PRODUCT_IMAGE();
        temp = temp.replaceAll(" ", "%20");
        Picasso.with(context).load(temp).into(holder.productImageView);

        holder.deleteItemLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteAlertDialog(cartArrayList.get(position));
            }
        });

        holder.editItemLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editAlertDialog(cartArrayList.get(position));
            }
        });

        if(position >lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.up_from_bottom);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }

        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return cartArrayList.size();
    }



    // Delete product Alert Dialog
    void deleteAlertDialog(final Cart cart){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Delete this item for cart. Are you sure?");
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        cartArrayList.remove(cart);
                        db.deleteItemFromCart(cart.getCART_PRODUCT_ID(),cart.getCART_PRODUCT_SIZE(),cart.getCART_PRODUCT_COLOR());
                        notifyDataSetChanged();
                        badgeTv.setText(""+db.getCartAllProductsCount());
                        if(db.getCartAllProductsCount()>0){
                            badgeTv.setVisibility(View.VISIBLE);
                        }else {
                            badgeTv.setVisibility(View.GONE);
                        }
                    }
                })
//                .setNeutralButton("Del", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                    }
//                })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    // Edit product alert dialog
    void editAlertDialog(final Cart cart){

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.edit_cart_item_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptView);
        TextView textView=(TextView)promptView.findViewById(R.id.edit_cart_item_title_textView);
        editCartSizeSpinner=(Spinner) promptView.findViewById(R.id.edit_cart_size_spinner);

        editCartQuantityEditText=(EditText) promptView.findViewById(R.id.edit_cart_quantity_editText);
        editCartDecrementQuantityButton=(ImageButton) promptView.findViewById(R.id.edit_cart_decrement_value_imageButton);
        editCartIncrementQuantityButton=(ImageButton) promptView.findViewById(R.id.edit_cart_increment_value_imageButton);
        textView.setText("Update: "+cart.getCART_PRODUCT_NAME());

        selectedQuantity=cart.getCART_PRODUCT_QUANTITY();
        editCartQuantityEditText.setText(""+selectedQuantity);
        editCartQuantityEditText.setSelection(editCartQuantityEditText.getText().length());
        setSizeSpinner(cart);


        editCartIncrementQuantityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedQuantity++;
                editCartQuantityEditText.setText(""+selectedQuantity);
                editCartQuantityEditText.setSelection(editCartQuantityEditText.getText().length());
            }
        });

        editCartDecrementQuantityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedQuantity>1){
                    selectedQuantity--;
                }else {
//                    Toast.makeText(context,"Delete from cart",Toast.LENGTH_LONG).show();
                }
                editCartQuantityEditText.setText(""+selectedQuantity);
                editCartQuantityEditText.setSelection(editCartQuantityEditText.getText().length());
            }
        });

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(!editCartQuantityEditText.getText().toString().equals("") && !editCartQuantityEditText.getText().toString().isEmpty() && !editCartQuantityEditText.getText().toString().equals("0")) {
                            selectedQuantity = Integer.parseInt(editCartQuantityEditText.getText().toString());
                            db.editCartItem(cart, editCartSizeSpinnerList.get(selectedSize), selectedQuantity);
                            cartArrayList = db.getCartProducts();
                            notifyDataSetChanged();
                            badgeTv.setText(""+db.getCartAllProductsCount());
                            if(db.getCartAllProductsCount()>0){
                                badgeTv.setVisibility(View.VISIBLE);
                            }else {
                                badgeTv.setVisibility(View.GONE);
                            }
                        }else if(editCartQuantityEditText.getText().toString().equals("0")){
//                            Toast.makeText(context,"Delete Item from cart",Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    // Getting products sizes from product arrayList with product ids

    void setSizeSpinner(Cart cart){

        editCartSizeSpinnerList=new ArrayList<String>();

        Product product=new Product();

        for(int i=0;i< GlobalValues.products.size();i++){
            if(cart.getCART_PRODUCT_ID() == GlobalValues.products.get(i).getPRODUCT_ID()){
                product=GlobalValues.products.get(i);
                break;
            }
        }

        for (int i=0;i<product.getSIZES().size();i++){
            editCartSizeSpinnerList.add(""+product.getSIZES().get(i).getCODE());
            if(cart.getCART_PRODUCT_SIZE().equals(""+product.getSIZES().get(i).getCODE())){
                selectedSize=i;
            }
        }

        ArrayAdapter<String> adp= new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,editCartSizeSpinnerList);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        editCartSizeSpinner.setAdapter(adp);
        editCartSizeSpinner.setSelection(selectedSize);

        editCartSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,int pos, long arg3) {
                selectedSize=pos;
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    void setQuantitySpinner(){

//        editCartQuantitySpinnerList=new ArrayList<String>();
//
//        if(selectedQuantity>=10) {
//            for (int i=0;i<=selectedQuantity; i++) {
//                editCartQuantitySpinnerList.add(i + "x");
//            }
//        }else {
//            for (int i=0; i<10; i++) {
//                editCartQuantitySpinnerList.add(i + "x");
//            }
//        }
//
//        ArrayAdapter<String> adp= new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,editCartQuantitySpinnerList);
//        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        editCartQuantitySpinner.setAdapter(adp);
//        editCartQuantitySpinner.setSelection(selectedQuantity);
//
//        editCartQuantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1,int pos, long arg3) {
//                selectedQuantity=pos;
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//                // TODO Auto-generated method stub
//            }
//        });
    }
}

