package alburraq.mobilecart.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import alburraq.mobilecart.R;

/**
 * Created by Al-Burraq Technologies on 4/12/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class ProductDetailImagesAdapter extends RecyclerView.Adapter<ProductDetailImagesAdapter.ViewHolder> {

    // Display Product Images on product detail image screen

    private ArrayList<String> productImagesArrayList;
    Context context;
    int lastPosition = -1;

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView productImageView;
        public CardView selected_product_parent;
        public ViewHolder(View v) {
            super(v);
            productImageView = (ImageView) v.findViewById(R.id.item_imageView);
            selected_product_parent =(CardView) v.findViewById(R.id.cardViewItemSelectionRowParent);
        }
    }

    public ProductDetailImagesAdapter(ArrayList<String> productImagesArrayList, Context context) {
        this.productImagesArrayList=productImagesArrayList;
        this.context=context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_detail_images, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String temp = productImagesArrayList.get(position);
        temp = temp.replaceAll(" ", "%20");
        Picasso.with(context).load(temp).into(holder.productImageView);

        if(position >lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.up_from_bottom);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }
        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return productImagesArrayList.size();
    }
}
