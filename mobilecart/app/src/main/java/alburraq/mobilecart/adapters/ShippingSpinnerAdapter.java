package alburraq.mobilecart.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import alburraq.mobilecart.R;
import alburraq.mobilecart.interfaces.SelectedShippingListener;
import alburraq.mobilecart.models.Shipping;

/**
 * Created by Al-Burraq Technologies on 4/17/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class ShippingSpinnerAdapter extends RecyclerView.Adapter<ShippingSpinnerAdapter.ViewHolder>{

    private ArrayList<Shipping> shippingArrayList;
    Context context;
    SelectedShippingListener selectedShippingListener = null;
    int lastPosition = -1;


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView shippingName;
        public TextView shippingDescription;
        public TextView shippingPrice;
        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            shippingName = (TextView) v.findViewById(R.id.shipping_title);
            shippingDescription = (TextView) v.findViewById(R.id.shipping_description);
            shippingPrice = (TextView) v.findViewById(R.id.shipping_price);
        }
        @Override
        public void onClick(View v) {
            if (selectedShippingListener != null) {
                selectedShippingListener.itemClicked(v, getAdapterPosition(),shippingArrayList);
            }
        }
    }

    public void setClickListener(SelectedShippingListener clicklistener) {
        this.selectedShippingListener = clicklistener;
    }

    public ShippingSpinnerAdapter(ArrayList<Shipping> shippingArrayList,Context context) {
        this.shippingArrayList=shippingArrayList;
        this.context=context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shipping_spinner_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.shippingName.setText(shippingArrayList.get(position).getName());
        holder.shippingDescription.setText(shippingArrayList.get(position).getDescription());
        int price=shippingArrayList.get(position).getPrice();
        holder.shippingPrice.setText("+ $"+price+".00");

//        if(position >lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.up_from_bottom);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }

        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return shippingArrayList.size();
    }

}
