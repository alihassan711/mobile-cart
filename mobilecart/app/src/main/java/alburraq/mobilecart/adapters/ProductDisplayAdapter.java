package alburraq.mobilecart.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import alburraq.mobilecart.interfaces.ClickListener;
import alburraq.mobilecart.R;
import alburraq.mobilecart.models.ColorList;
import alburraq.mobilecart.models.ColorsImage;
import alburraq.mobilecart.models.Price;
import alburraq.mobilecart.models.Product;
import alburraq.mobilecart.utils.GlobalValues;

/**
 * Created by Al-Burraq Technologies on 4/12/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class ProductDisplayAdapter extends RecyclerView.Adapter<ProductDisplayAdapter.ViewHolder> implements Filterable {

    //Display products by category selected from navigation drawer main fragment

    private ArrayList<Product> productArrayList;
    private ArrayList<Product> originalProductArrayList;
    Context context;
    private ClickListener clicklistener = null;
    private ProductDisplayAdapter.ValueFilter valueFilter;
    int lastPosition = -1;

    // Search Filter
    @Override
    public Filter getFilter() {
        if(valueFilter==null) {
            valueFilter=new ProductDisplayAdapter.ValueFilter();
        }
        return valueFilter;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        public TextView productName;
        public TextView productPrice;
        public ImageView productImageView;
        public CardView selected_product_parent;
        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            productImageView = (ImageView) v.findViewById(R.id.item_imageView);
            productName = (TextView) v.findViewById(R.id.textView_item_description);
            productPrice = (TextView) v.findViewById(R.id.textView_item_price);
            selected_product_parent =(CardView) v.findViewById(R.id.cardViewItemSelectionRowParent);
        }

        @Override
        public void onClick(View v) {
            if (clicklistener != null) {
                clicklistener.itemClicked(v, getAdapterPosition(),productArrayList);
            }
        }
    }

    public void setClickListener(ClickListener clicklistener) {
        this.clicklistener = clicklistener;
    }

    public ProductDisplayAdapter(ArrayList<Product> productArrayList, Context context) {
        this.productArrayList=productArrayList;
        this.originalProductArrayList=productArrayList;
        this.context=context;
    }


    @Override
    public ProductDisplayAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row, parent, false);
        ProductDisplayAdapter.ViewHolder vh = new ProductDisplayAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ProductDisplayAdapter.ViewHolder holder, final int position) {

        holder.productName.setText(productArrayList.get(position).getPRODUCT_NAME());

        Price price=productArrayList.get(position).getPRICE();
        holder.productPrice.setText("$"+price.getPRICE()+".00");

        if(productArrayList.get(position).getCOLORS().size()>0) {
            List<ColorList> colorList = productArrayList.get(position).getCOLORS();
            List<ColorsImage> colorsImageList = colorList.get(0).getIMAGES();
            String temp = colorsImageList.get(0).getTHUMBNAIL();
            temp = temp.replaceAll(" ", "%20");
            Picasso.with(context).load(temp).into(holder.productImageView);
        }

        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }



    private class ValueFilter extends Filter {
        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if(constraint!=null && constraint.length()>0){
                ArrayList<Product> filterList=new ArrayList<Product>();
                for(int i = 0; i< GlobalValues.products.size(); i++){
                        if( GlobalValues.products.get(i).getPRODUCT_NAME().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                                GlobalValues.products.get(i).getDESCRIPTION().toLowerCase().contains(constraint.toString().toLowerCase()) ) {
                            filterList.add(GlobalValues.products.get(i));
                    }

                }
                results.count=filterList.size();
                results.values=filterList;
            }else{
                results.count=originalProductArrayList.size();
                ArrayList<Product> filterList=new ArrayList<Product>();
                for(int i=0; i<results.count;i++){
                    filterList.add(originalProductArrayList.get(i));
                }
                results.values=filterList;
            }
            return results;
        }
        //Invoked in the UI thread to publish the filtering results in the user interface
        @Override
        protected void publishResults(CharSequence constraint,
                                      Filter.FilterResults results) {
            productArrayList=(ArrayList<Product>) results.values;
            notifyDataSetChanged();
        }
    }

}
