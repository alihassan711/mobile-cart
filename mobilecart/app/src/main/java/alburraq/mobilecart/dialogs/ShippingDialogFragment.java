package alburraq.mobilecart.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import alburraq.mobilecart.R;
import alburraq.mobilecart.activities.SplashActivity;
import alburraq.mobilecart.adapters.ShippingSpinnerAdapter;
import alburraq.mobilecart.interfaces.SelectedShippingListener;
import alburraq.mobilecart.interfaces.ShippingDialogInterface;
import alburraq.mobilecart.models.MenuItems;
import alburraq.mobilecart.models.Shipping;
import alburraq.mobilecart.utils.GlobalValues;
import alburraq.mobilecart.utils.JSONParser;

/**
 * Created by Al-Burraq Technologies on 4/17/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class ShippingDialogFragment extends DialogFragment implements SelectedShippingListener {

    private ProgressBar progressBar;

    private ShippingDialogInterface shippingDialogInterface;
    ShippingSpinnerAdapter shippingSpinnerAdapter;
    ImageView closeBtn;


    private Fragment thisFragment;
    private RecyclerView shippingList;
    GridLayoutManager gridLayoutManager;
    private Shipping selectedShippingType;

    public static ShippingDialogFragment newInstance( Shipping selectedShipping, ShippingDialogInterface shippingDialogInterface) {
        ShippingDialogFragment frag = new ShippingDialogFragment();
        frag.selectedShippingType = selectedShipping;
        frag.shippingDialogInterface = shippingDialogInterface;
        return frag;
    }

    // ShippingDialogFragment instance to pass interface
    public static ShippingDialogFragment newInstance(ShippingDialogInterface shippingDialogInterface) {
        ShippingDialogFragment frag = new ShippingDialogFragment();
        frag.shippingDialogInterface = shippingDialogInterface;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        thisFragment = this;
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialogFullscreen);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Window window = d.getWindow();
            window.setLayout(width, height);
            window.setWindowAnimations(R.style.alertDialogAnimation);
        }
    }

    // Creating Shipping Dialog View for selecting Shipping Method
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shipping_delivery, container, false);

        shippingList = (RecyclerView) view.findViewById(R.id.shipping_dialog_list);
        gridLayoutManager = new GridLayoutManager(getActivity(),1);
        shippingList.setLayoutManager(gridLayoutManager);
        progressBar = (ProgressBar) view.findViewById(R.id.shipping_dialog_progressBar);
        closeBtn = (ImageView) view.findViewById(R.id.shipping_dialog_close);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        setContentVisible(true);

        setAdapter();


        return view;
    }

    private void setContentVisible(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.GONE);
            shippingList.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            shippingList.setVisibility(View.GONE);
        }
    }


    void setAdapter(){
//		Get Shipping Methods from json file or get from server and save in arrayList
//        getShippingMethods();
        getShippingMethodsFromServer();
    }


    public void onShippingSelected(Shipping selectedShipping) {
        if (shippingDialogInterface != null)
            shippingDialogInterface.onShippingSelected(selectedShipping);
        dismiss();
    }

    @Override
    public void itemClicked(View view, int position, List<Shipping> shippingArrayList) {
        Shipping selectedShipping = shippingArrayList.get(position);
        onShippingSelected(selectedShipping);
    }


    void getShippingMethods(){

        GlobalValues.shippingArrayList=new ArrayList<>();
        //Reading Shipping Methods from JSON file

        InputStream is = getResources().openRawResource(R.raw.shipping_methods);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        Reader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


		/*
		 * Here is Shipping Methods response string get from file (if you are reading from server,
		 * server should return same response as in file otherwise you have to make changes in parsing response)
		 */

        String response = writer.toString();

        try {
            JSONObject jsonObjResponse = new JSONObject(response);
            JSONArray shipping_methodsJsonArray=jsonObjResponse.getJSONArray("shipping_methods");
            for (int i=0;i<shipping_methodsJsonArray.length();i++){
                JSONObject jsonObj = shipping_methodsJsonArray.getJSONObject(i);
                Shipping shipping=new Shipping();
                shipping.setName(jsonObj.getString("NAME"));
                shipping.setDescription(jsonObj.getString("DESCRIPTION"));
                shipping.setPrice(jsonObj.getInt("PRICE"));


                // Add data in shipping ArrayList
                GlobalValues.shippingArrayList.add(shipping);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void getShippingMethodsFromServer() {

        GlobalValues.shippingArrayList=new ArrayList<>();

        new AsyncTask<String, Void, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected JSONObject doInBackground(String... strings) {
                JSONParser parser = new JSONParser();
                JSONObject JObject = parser.makeHttpRequest(GlobalValues.BASE_URL + "getShippings", "GET", null);
                return JObject;
            }

            @Override
            protected void onPostExecute(JSONObject JObject) {
                if (JObject != null) {
                    try {

                            if(JObject.has("shipping_methods")) {
                                JSONArray jsonArray = JObject.getJSONArray("shipping_methods");
                                for(int i=0;i<jsonArray.length();i++) {

                                    JSONObject jsonObj = jsonArray.getJSONObject(i);
                                    Shipping shipping=new Shipping();
                                    shipping.setName(jsonObj.getString("name"));
                                    shipping.setDescription(jsonObj.getString("description"));
                                    shipping.setPrice(Integer.parseInt(jsonObj.getString("price")));

                                    // Add data in shipping ArrayList
                                    GlobalValues.shippingArrayList.add(shipping);

                                }

                                shippingSpinnerAdapter=new ShippingSpinnerAdapter(GlobalValues.shippingArrayList,getContext());
                                shippingList.setAdapter(shippingSpinnerAdapter);
                                shippingSpinnerAdapter.setClickListener(ShippingDialogFragment.this);
                            }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }
}
