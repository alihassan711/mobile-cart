package alburraq.mobilecart.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import alburraq.mobilecart.R;
import alburraq.mobilecart.activities.LandingActivity;
import alburraq.mobilecart.models.LoggedInUser;
import alburraq.mobilecart.utils.GlobalValues;
import alburraq.mobilecart.utils.JSONParser;

/**
 * Created by Al-Burraq Technologies on 4/14/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class RegisterFragment extends Fragment {

	EditText editTextFirstName;
	EditText editTextPhone;
	EditText editTextEmail;
	EditText editTextPassword;
	EditText editTextConfirmPassword;
	Button button_register;
	ProgressBar progressBar;

	public RegisterFragment() {
	}



	//*******************PROGRESS******************************
	private ProgressDialog mSpinner;

	private void showSpinner() {
		mSpinner = new ProgressDialog(getContext());
		mSpinner.setTitle("Please wait...");
		mSpinner.show();
		mSpinner.setCancelable(false);
		mSpinner.setCanceledOnTouchOutside(false);
	}

	private void DismissSpinner(){
		if(mSpinner!=null){
			mSpinner.dismiss();
		}
	}
	//*******************PROGRESS******************************

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_register, container,
				false);

//		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		button_register = (Button) rootView.findViewById(R.id.button_register);
		editTextFirstName = (EditText) rootView.findViewById(R.id.editText_name);
		editTextPhone = (EditText) rootView.findViewById(R.id.editText_mobile);
		editTextEmail = (EditText) rootView.findViewById(R.id.editText_email);
		editTextPassword = (EditText) rootView.findViewById(R.id.editText_password);
		editTextConfirmPassword = (EditText) rootView.findViewById(R.id.editText_confirmPassword);
		progressBar=(ProgressBar)rootView.findViewById(R.id.native_progress_bar);


		button_register.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(editTextFirstName.getText().toString().isEmpty()){
					editTextFirstName.setError("Enter First name");
					Toast.makeText(getContext(),"Enter First name", Toast.LENGTH_LONG).show();
					return;
				}

				if(editTextPhone.getText().toString().isEmpty()){
					editTextPhone.setError("Enter Contact info");
					Toast.makeText(getContext(),"Enter Contact info", Toast.LENGTH_LONG).show();
					return;
				}


				if(editTextEmail.getText().toString().isEmpty()){
					editTextEmail.setError("Enter Email");
					Toast.makeText(getContext(),"Enter Email", Toast.LENGTH_LONG).show();
					return;
				}

				if(editTextPassword.getText().toString().isEmpty()){
					editTextPassword.setError("Enter Password");
					Toast.makeText(getContext(),"Password Cannot be Empty ", Toast.LENGTH_LONG).show();
					return;
				}

				if(editTextConfirmPassword.getText().toString().isEmpty()){
					editTextPassword.setError("Confirm Your Password");
					Toast.makeText(getContext(),"Confirm Password Cannot be Empty ", Toast.LENGTH_LONG).show();
					return;
				}

				if(!editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())){
					Toast.makeText(getContext(),"Password not matched", Toast.LENGTH_LONG).show();
					return;
				}

				if(!GlobalValues.isNetworkAvailable(getContext())){
					Toast.makeText(getContext(),"No internet access",Toast.LENGTH_LONG).show();
					return;
				}

				String firstName=editTextFirstName.getText().toString();
				String lastName="optional ";
				String email=editTextEmail.getText().toString();
				String password=editTextPassword.getText().toString();
				String dateTime=""+System.currentTimeMillis() % 1000;

				registerUser(firstName,lastName,email,password,dateTime);
			}
		});

		return rootView;
	}

	public void registerUser(String firstName, String lastName, String email, final String password, String dateTime) {

		new AsyncTask<String, Void, JSONObject>(){

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				if(progressBar!=null){
					progressBar.setVisibility(View.VISIBLE);
				}
				showSpinner();
			}

			@Override
			protected JSONObject doInBackground(String... params) {
				JSONParser parser=new JSONParser();
				HashMap<String,String> hash=new HashMap<String, String>();
				hash.put("method","signUp");
				hash.put("nonce","b919aee7bd");
				hash.put("firstName",params[0]);
				hash.put("lastName",params[1]);
				hash.put("displayName",params[0]+" "+params[1]);
				hash.put("email",params[2]);
				hash.put("userPassword",params[3]);
				hash.put("datetime",params[4]);
				JSONObject JObject = parser.makeHttpRequest(GlobalValues.BASE_URL+"signUp","POST",hash);
				return JObject;
			}

			@Override
			protected void onPostExecute(JSONObject JObject) {

				DismissSpinner();
				if(progressBar!=null){
					progressBar.setVisibility(View.VISIBLE);
				}

				String status=null;
				if(JObject!=null){
					try {
						status=JObject.get("success").toString();
						if(status.equals("0")){
							String msg=JObject.get("message").toString();
							Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();
							return;
						}
						else if(status.equals("1")){
							if(JObject.has("user")){
								GlobalValues.user = new LoggedInUser(JObject.getJSONObject("user"));
								GlobalValues.setUserLoginStatus(getActivity(),true);
								Intent intent = new Intent(getActivity(), LandingActivity.class);
								startActivity(intent);
								getActivity().finish();
							}
							else
							{
								Toast.makeText(getContext(),"No data found",Toast.LENGTH_LONG).show();
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				else{
					Toast.makeText(getContext(),"Error occured while accuring data",Toast.LENGTH_LONG).show();
				}
			}
		}.execute(firstName,lastName,email,password,dateTime);
	}

}