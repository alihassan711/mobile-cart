package alburraq.mobilecart.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import alburraq.mobilecart.activities.LandingActivity;
import alburraq.mobilecart.R;
import alburraq.mobilecart.models.Brand;
import alburraq.mobilecart.models.Category;
import alburraq.mobilecart.models.Collection;
import alburraq.mobilecart.models.ColorList;
import alburraq.mobilecart.models.ColorsImage;
import alburraq.mobilecart.models.LoggedInUser;
import alburraq.mobilecart.models.MenuItems;
import alburraq.mobilecart.models.Price;
import alburraq.mobilecart.models.Product;
import alburraq.mobilecart.models.Shipping;
import alburraq.mobilecart.models.Size;
import alburraq.mobilecart.models.StockKeepingUnit;
import alburraq.mobilecart.models.User;
import alburraq.mobilecart.utils.GlobalValues;
import alburraq.mobilecart.utils.JSONParser;

/**
 * Created by Al-Burraq Technologies on 4/4/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class LoginFragment extends Fragment {

	Button button_login;
	private CallbackManager callbackManager;
	private LoginButton facebookLoginButton;
	String fb_email,fb_name,fb_id,fb_url;
	private TwitterLoginButton twitterLoginButton;
	EditText editTextEmail;
	EditText editTextPassword;


	public LoginFragment() {
	}




	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_login, container,
					false);

		button_login = (Button) rootView.findViewById(R.id.button_login);
		editTextEmail = (EditText) rootView.findViewById(R.id.editText_email);
		editTextPassword = (EditText) rootView.findViewById(R.id.editText_password);
		button_login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(editTextEmail.getText().toString().isEmpty()){
					Toast.makeText(getContext(),"Enter Email", Toast.LENGTH_LONG).show();
					return;
				}

				if(editTextPassword.getText().toString().isEmpty()){
					Toast.makeText(getContext(),"Password Cannot be Empty ", Toast.LENGTH_LONG).show();
					return;
				}

				if(!GlobalValues.isNetworkAvailable(getContext())){
					Toast.makeText(getContext(),"No internet access",Toast.LENGTH_LONG).show();
					return;
				}

				String email=editTextEmail.getText().toString();
				String password=editTextPassword.getText().toString();

				loginUserResponse(email,password);

			}
		});

//		Note: Your facebook_app_id should be changed in your source code before using.
		facebookLogin(rootView);
//		Note: Your consumer key and secret should be changed in your source code before using.
		twitterLogin(rootView);
//		Get Products from json file or get from server and save in arrayList
//		getProducts();
//		Get Recommended Products from json file or get from server and save in arrayList
//		getRecommendedProducts();
//		Get Menu Navigation Drawer data from json file or get from server and save in arrayList

//		if(GlobalValues.isNetworkAvailable(getActivity())){
//			getMenusFromServer();
//		}else {
////			getMenu();
//		}


		return rootView;
	}

	void getProducts(){

		GlobalValues.products=new ArrayList<>();
		// Reading JSON file to get recommendedProducts data
		InputStream is = getResources().openRawResource(R.raw.product);
		Writer writer = new StringWriter();
		char[] buffer = new char[1024];
		Reader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int n;
			while ((n = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, n);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/*
		 * Here is recommendedProducts response string get from file (if you are reading from server,
		 * server should return same response as in file otherwise you have to make changes in parsing response)
		 */

		String response = writer.toString();

		try {
			JSONObject jsonObjResponse = new JSONObject(response);
			JSONArray productsJsonArray=jsonObjResponse.getJSONArray("recommendedProducts");

			for(int i=0;i<productsJsonArray.length();i++) {

				JSONObject jsonObj=productsJsonArray.getJSONObject(i);
				Product product = new Product();

				product.setPRODUCT_NAME(jsonObj.getString("PRODUCT_NAME"));
				product.setBEST_SALES_RANK(jsonObj.getInt("BEST_SALES_RANK"));
				product.setPRODUCT_ID(jsonObj.getInt("PRODUCT_ID"));
				product.setITEM_GROUP_ID(jsonObj.getInt("ITEM_GROUP_ID"));
				product.setIS_NEW(jsonObj.getBoolean("IS_NEW"));
				product.setSALE(jsonObj.getBoolean("SALE"));

				Brand brand = new Brand();
				JSONObject brandJsonObject = jsonObj.getJSONObject("BRAND");
				brand.setBRAND_ID(brandJsonObject.getInt("BRAND_ID"));
				brand.setBRAND_NAME(brandJsonObject.getString("BRAND_NAME"));
				brand.setBRAND_IMG(brandJsonObject.getString("BRAND_IMG"));
				brand.setIS_PREMIUM(brandJsonObject.getBoolean("IS_PREMIUM"));
				product.setBRAND(brand);

				product.setDESCRIPTION(jsonObj.getString("DESCRIPTION"));
				product.setPRODUCT_URL(jsonObj.getString("PRODUCT_URL"));

				Price price = new Price();
				JSONObject priceJsonObject = jsonObj.getJSONObject("PRICE");
				price.setPRICE(priceJsonObject.getInt("PRICE"));
				price.setPROMO_PRICE(priceJsonObject.getInt("PROMO_PRICE"));
				product.setPRICE(price);

				List<Size> sizeList=new ArrayList<>();
				JSONArray sizesJsonArray = jsonObj.getJSONArray("SIZES");
				for (int s=0;s<sizesJsonArray.length();s++){
					JSONObject sizeJsonObject=sizesJsonArray.getJSONObject(s);
					Size size=new Size();
					size.setID(sizeJsonObject.getInt("ID"));
					size.setCODE(sizeJsonObject.getInt("CODE"));
					size.setNAME(sizeJsonObject.getString("NAME"));
					sizeList.add(size);
				}
				product.setSIZES(sizeList);

				List<ColorList> colorList=new ArrayList<>();
				JSONArray colorsJsonArray = jsonObj.getJSONArray("COLORS");
				for (int c=0;c<colorsJsonArray.length();c++){
					JSONObject colorsJsonObject=colorsJsonArray.getJSONObject(c);
					ColorList color=new ColorList();
					color.setID(colorsJsonObject.getInt("ID"));
					color.setNAME(colorsJsonObject.getString("NAME"));
					color.setCODE(""+colorsJsonObject.getInt("CODE"));
					color.setRGB(colorsJsonObject.getString("RGB"));

					List<ColorsImage> colorsImageList=new ArrayList<>();
					JSONArray colorsImageJsonArray = colorsJsonObject.getJSONArray("COLORS_IMAGE");
					for (int ci=0;ci<colorsImageJsonArray.length();ci++){
						JSONObject colorImageJsonObject=colorsImageJsonArray.getJSONObject(ci);
						ColorsImage colorsImage= new ColorsImage();
						colorsImage.setIMAGE(colorImageJsonObject.getString("IMAGE"));
						colorsImage.setTHUMBNAIL(colorImageJsonObject.getString("THUMBNAIL"));
						colorsImageList.add(colorsImage);
					}
					color.setIMAGES(colorsImageList);

					colorList.add(color);
				}
				product.setCOLORS(colorList);

				List<Collection> collectionList=new ArrayList<>();
				JSONArray collectionJsonArray=jsonObj.getJSONArray("COLLECTIONS");
				for (int col=0;col<collectionJsonArray.length();col++){
					JSONObject collectionJsonObject=collectionJsonArray.getJSONObject(col);
					Collection collection=new Collection();
					collection.setCOLLECTION_ID(collectionJsonObject.getInt("COLLECTION_ID"));
					collection.setCOLLECTIONTEXT(collectionJsonObject.getString("COLLECTIONTEXT"));
					collectionList.add(collection);
				}
				product.setCOLLECTIONS(collectionList);

				List<Category> categoryList=new ArrayList<>();
				JSONArray categoriesJsonArray=jsonObj.getJSONArray("CATEGORIES");
				for (int cat=0;cat<categoriesJsonArray.length();cat++){
					JSONObject categoryJsonObject=categoriesJsonArray.getJSONObject(cat);
					Category category=new Category();
					category.setCATEGORYID(categoryJsonObject.getInt("CATEGORYID"));
					category.setCATEGORYTEXT(categoryJsonObject.getString("CATEGORYTEXT"));
					categoryList.add(category);
				}
				product.setCATEGORIES(categoryList);

				List<StockKeepingUnit> stockKeepingUnitList=new ArrayList<>();
				JSONArray skuJsonArray=jsonObj.getJSONArray("SKUS");
				for (int sk=0;sk<skuJsonArray.length();sk++){
					JSONObject skuJsonObject=skuJsonArray.getJSONObject(sk);
					StockKeepingUnit stockKeepingUnit=new StockKeepingUnit();
					stockKeepingUnit.setID(skuJsonObject.getInt("ID"));
					stockKeepingUnit.setPCS_IN_STOCK(skuJsonObject.getInt("PCS_IN_STOCK"));
					stockKeepingUnit.setCOLOR_ID(skuJsonObject.getInt("COLOR_ID"));
					stockKeepingUnit.setSIZE_ID(skuJsonObject.getInt("SIZE_ID"));

					stockKeepingUnitList.add(stockKeepingUnit);
				}
				product.setSKUS(stockKeepingUnitList);

				// add single product data in product arrayList for later use
				GlobalValues.products.add(product);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


	void getRecommendedProducts(){

		GlobalValues.recommendedProducts=new ArrayList<>();
		// Reading JSON file to get recommended recommendedProducts
		InputStream is = getResources().openRawResource(R.raw.recommended_products);
		Writer writer = new StringWriter();
		char[] buffer = new char[1024];
		Reader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int n;
			while ((n = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, n);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}


		/*
		 * Here is recommended recommendedProducts response string get from file (if you are reading from server,
		 * server should return same response as in file otherwise you have to make changes in parsing response)
		 */

		String response = writer.toString();

		try {
			JSONObject jsonObjResponse = new JSONObject(response);
			JSONArray productsJsonArray=jsonObjResponse.getJSONArray("recommendedProducts");

			for(int i=0;i<productsJsonArray.length();i++) {

				JSONObject jsonObj=productsJsonArray.getJSONObject(i);
				Product product = new Product();

				product.setPRODUCT_NAME(jsonObj.getString("PRODUCT_NAME"));
				product.setBEST_SALES_RANK(jsonObj.getInt("BEST_SALES_RANK"));
				product.setPRODUCT_ID(jsonObj.getInt("PRODUCT_ID"));
				product.setITEM_GROUP_ID(jsonObj.getInt("ITEM_GROUP_ID"));
				product.setIS_NEW(jsonObj.getBoolean("IS_NEW"));
				product.setSALE(jsonObj.getBoolean("SALE"));

				Brand brand = new Brand();
				JSONObject brandJsonObject = jsonObj.getJSONObject("BRAND");
				brand.setBRAND_ID(brandJsonObject.getInt("BRAND_ID"));
				brand.setBRAND_NAME(brandJsonObject.getString("BRAND_NAME"));
				brand.setBRAND_IMG(brandJsonObject.getString("BRAND_IMG"));
				brand.setIS_PREMIUM(brandJsonObject.getBoolean("IS_PREMIUM"));
				product.setBRAND(brand);

				product.setDESCRIPTION(jsonObj.getString("DESCRIPTION"));
				product.setPRODUCT_URL(jsonObj.getString("PRODUCT_URL"));

				Price price = new Price();
				JSONObject priceJsonObject = jsonObj.getJSONObject("PRICE");
				price.setPRICE(priceJsonObject.getInt("PRICE"));
				price.setPROMO_PRICE(priceJsonObject.getInt("PROMO_PRICE"));
				product.setPRICE(price);

				List<Size> sizeList=new ArrayList<>();
				JSONArray sizesJsonArray = jsonObj.getJSONArray("SIZES");
				for (int s=0;s<sizesJsonArray.length();s++){
					JSONObject sizeJsonObject=sizesJsonArray.getJSONObject(s);
					Size size=new Size();
					size.setID(sizeJsonObject.getInt("ID"));
					size.setCODE(sizeJsonObject.getInt("CODE"));
					size.setNAME(sizeJsonObject.getString("NAME"));
					sizeList.add(size);
				}
				product.setSIZES(sizeList);

				List<ColorList> colorList=new ArrayList<>();
				JSONArray colorsJsonArray = jsonObj.getJSONArray("COLORS");
				for (int c=0;c<colorsJsonArray.length();c++){
					JSONObject colorsJsonObject=colorsJsonArray.getJSONObject(c);
					ColorList color=new ColorList();
					color.setID(colorsJsonObject.getInt("ID"));
					color.setNAME(colorsJsonObject.getString("NAME"));
					color.setCODE(""+colorsJsonObject.getInt("CODE"));
					color.setRGB(colorsJsonObject.getString("RGB"));

					List<ColorsImage> colorsImageList=new ArrayList<>();
					JSONArray colorsImageJsonArray = colorsJsonObject.getJSONArray("COLORS_IMAGE");
					for (int ci=0;ci<colorsImageJsonArray.length();ci++){
						JSONObject colorImageJsonObject=colorsImageJsonArray.getJSONObject(ci);
						ColorsImage colorsImage= new ColorsImage();
						colorsImage.setIMAGE(colorImageJsonObject.getString("IMAGE"));
						colorsImage.setTHUMBNAIL(colorImageJsonObject.getString("THUMBNAIL"));
						colorsImageList.add(colorsImage);
					}
					color.setIMAGES(colorsImageList);

					colorList.add(color);
				}
				product.setCOLORS(colorList);

				List<Collection> collectionList=new ArrayList<>();
				JSONArray collectionJsonArray=jsonObj.getJSONArray("COLLECTIONS");
				for (int col=0;col<collectionJsonArray.length();col++){
					JSONObject collectionJsonObject=collectionJsonArray.getJSONObject(col);
					Collection collection=new Collection();
					collection.setCOLLECTION_ID(collectionJsonObject.getInt("COLLECTION_ID"));
					collection.setCOLLECTIONTEXT(collectionJsonObject.getString("COLLECTIONTEXT"));
					collectionList.add(collection);
				}
				product.setCOLLECTIONS(collectionList);

				List<Category> categoryList=new ArrayList<>();
				JSONArray categoriesJsonArray=jsonObj.getJSONArray("CATEGORIES");
				for (int cat=0;cat<categoriesJsonArray.length();cat++){
					JSONObject categoryJsonObject=categoriesJsonArray.getJSONObject(cat);
					Category category=new Category();
					category.setCATEGORYID(categoryJsonObject.getInt("CATEGORYID"));
					category.setCATEGORYTEXT(categoryJsonObject.getString("CATEGORYTEXT"));
					categoryList.add(category);
				}
				product.setCATEGORIES(categoryList);

				List<StockKeepingUnit> stockKeepingUnitList=new ArrayList<>();
				JSONArray skuJsonArray=jsonObj.getJSONArray("SKUS");
				for (int sk=0;sk<skuJsonArray.length();sk++){
					JSONObject skuJsonObject=skuJsonArray.getJSONObject(sk);
					StockKeepingUnit stockKeepingUnit=new StockKeepingUnit();
					stockKeepingUnit.setID(skuJsonObject.getInt("ID"));
					stockKeepingUnit.setPCS_IN_STOCK(skuJsonObject.getInt("PCS_IN_STOCK"));
					stockKeepingUnit.setCOLOR_ID(skuJsonObject.getInt("COLOR_ID"));
					stockKeepingUnit.setSIZE_ID(skuJsonObject.getInt("SIZE_ID"));

					stockKeepingUnitList.add(stockKeepingUnit);
				}
				product.setSKUS(stockKeepingUnitList);

				// Adding data in recommended recommendedProducts arrayList for later use

				GlobalValues.recommendedProducts.add(product);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


	void getMenu(){

		GlobalValues.menuItemsArrayList=new ArrayList<>();
		//Reading menu data for Navigation drawer from JSON file

		InputStream is = getResources().openRawResource(R.raw.menu);
		Writer writer = new StringWriter();
		char[] buffer = new char[1024];
		Reader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int n;
			while ((n = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, n);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}


		/*
		 * Here is menu data response string get from file (if you are reading from server,
		 * server should return same response as in file otherwise you have to make changes in parsing response)
		 */

		String response = writer.toString();

		try {
				JSONObject jsonObjResponse = new JSONObject(response);
				JSONArray menuJsonArray=jsonObjResponse.getJSONArray("menu");
				for(int i=0;i<menuJsonArray.length();i++) {

					JSONObject jsonObj = menuJsonArray.getJSONObject(i);
					MenuItems menuItems = new MenuItems();

					menuItems.setCATEGORY_ID(jsonObj.getInt("CATEGORY_ID"));
					menuItems.setCATEGORY_PARENT_ID(jsonObj.getInt("PARENT_ID"));
					menuItems.setCATEGORY_TITLE(jsonObj.getString("CATEGORY_NAME"));
					menuItems.setCATEGORY_PRIORITY(jsonObj.getBoolean("CATEGORY_PRIORITY"));

					// add categories and sub-categories data into menu items arrayList for later use
					GlobalValues.menuItemsArrayList.add(menuItems);
				}

			} catch (JSONException e) {
			e.printStackTrace();
		}

	}



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Adding the login result back to the callbackManager button
		callbackManager.onActivityResult(requestCode, resultCode, data);
		//Adding the login result back to the twitterLoginButton button
		twitterLoginButton.onActivityResult(requestCode, resultCode, data);
	}

	//Facebook

	void facebookLogin(View view){
		callbackManager = CallbackManager.Factory.create();
		facebookLoginButton = (LoginButton) view.findViewById(R.id.facebook_login_button);
		facebookLoginButton.setReadPermissions("user_friends","email","public_profile");
		facebookLoginButton.setFragment(this);
		facebookLoginButton.registerCallback(callbackManager, callback);
	}

	private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
		@Override
		public void onSuccess(LoginResult loginResult) {
			AccessToken accessToken = loginResult.getAccessToken();
			Profile profile = Profile.getCurrentProfile();

			GraphRequest request = GraphRequest.newMeRequest(
					loginResult.getAccessToken(),
					new GraphRequest.GraphJSONObjectCallback() {
						@Override
						public void onCompleted(
								JSONObject object,
								GraphResponse response) {
							try {
								if (object.has("email") || (object.has("id") && object.has("name"))) {
									fb_name = object.getString("name");
									fb_id = object.getString("id");
									fb_url = object.getJSONObject("picture").getJSONObject("data").getString("url");

									try {
										fb_email = object.getString("email");
									} catch (Exception e) {
										String newEmail=fb_name.replace(" ","_");
										fb_email = newEmail+"129@gmail.com";
									}


									socialLogin(fb_name,"",fb_email,fb_url);

								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});


			Bundle parameters = new Bundle();
			parameters.putString("fields", "email,id,name,link,picture");
			request.setParameters(parameters);
			request.executeAsync();
		}

		@Override
		public void onCancel() {

		}

		@Override
		public void onError(FacebookException e) {

		}
	};

	//Facebook


	//Twitter

	void twitterLogin(View view){
		twitterLoginButton = (TwitterLoginButton) view.findViewById(R.id.twitterLogin_button);
		//Adding callback to the button
		twitterLoginButton.setCallback(new Callback<TwitterSession>() {
			@Override
			public void success(Result<TwitterSession> result) {
				//If login succeeds passing the Calling the login method and passing Result object
				login(result);
			}

			@Override
			public void failure(TwitterException exception) {
				//If failure occurs while login handle it here
				Log.d("TwitterKit", "Login with Twitter failure", exception);
			}
		});
	}


	//The login function accepting the result object
	public void login(Result<TwitterSession> result) {
		//Creating a twitter session with result's data
		TwitterSession session = result.data;
		//Getting the username from session
		final String username = session.getUserName();
		fb_name = session.getUserName();

		fb_url = "";
		String newEmail=fb_name.replace(" ","_");
		fb_email = newEmail+"129@gmail.com";


		socialLogin(fb_name,"",fb_email,fb_url);
//		Intent intent = new Intent(getActivity(), LandingActivity.class);
//		startActivity(intent);
//		getActivity().finish();
	}
	//Twitter



	public void socialLogin(String firstName,String lastName, String email, String imageURL) {

		new AsyncTask<String, Void, JSONObject>(){

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected JSONObject doInBackground(String... params) {
				JSONParser parser=new JSONParser();
				HashMap<String,String> hash=new HashMap<String, String>();
				hash.put("first_name",params[0]);
				hash.put("last_name",params[1]);
				hash.put("email",params[2]);
				hash.put("image",params[3]);
				JSONObject JObject = parser.makeHttpRequest(GlobalValues.BASE_URL+"socialLogin","POST",hash);
				return JObject;
			}

			@Override
			protected void onPostExecute(JSONObject JObject) {


				String status=null;
				if(JObject!=null){
					try {
						status=JObject.get("success").toString();
						if(status.equals("0")){
							String msg=JObject.get("message").toString();
							Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();
							return;
						}
						else if(status.equals("1")){
							if(JObject.has("user")){
								GlobalValues.user = new LoggedInUser(JObject.getJSONObject("user"));
								GlobalValues.setUserLoginStatus(getActivity(),true);
								Intent intent = new Intent(getActivity(), LandingActivity.class);
								startActivity(intent);
								getActivity().finish();
							}
							else
							{
								Toast.makeText(getContext(),"No data found",Toast.LENGTH_LONG).show();
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				else{
					Toast.makeText(getContext(),"Error occured while accuring data",Toast.LENGTH_LONG).show();
				}
			}
		}.execute(firstName,lastName,email,imageURL);
	}

	public void loginUserResponse(String email,String password) {

		new AsyncTask<String, Void, JSONObject>(){

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected JSONObject doInBackground(String... params) {
				JSONParser parser=new JSONParser();
				HashMap<String,String> hash=new HashMap<String, String>();
				hash.put("method","signIn");
				hash.put("userName",params[0]);
				hash.put("password",params[1]);
				JSONObject JObject = parser.makeHttpRequest(GlobalValues.BASE_URL,"POST",hash);
				return JObject;
			}

			@Override
			protected void onPostExecute(JSONObject JObject) {
				String status=null;
				if(JObject!=null){
					try {
						status=JObject.get("success").toString();
						if(status.equals("0")){
							String msg=JObject.get("message").toString();
							Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();
							return;
						}
						else if(status.equals("1")){
							if(JObject.has("user")){
								GlobalValues.user = new LoggedInUser(JObject.getJSONObject("user"));
								GlobalValues.setUserLoginStatus(getActivity(),true);
								Intent intent = new Intent(getActivity(), LandingActivity.class);
								startActivity(intent);
								getActivity().finish();
							}
							else
							{
								Toast.makeText(getContext(),"No data found",Toast.LENGTH_LONG).show();
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				else{
					Toast.makeText(getContext(),"Error occured while accuring data",Toast.LENGTH_LONG).show();
				}
			}
		}.execute(email,password);
	}

}