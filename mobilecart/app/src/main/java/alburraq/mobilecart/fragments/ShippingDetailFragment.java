package alburraq.mobilecart.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.ArrayList;

import alburraq.mobilecart.R;
import alburraq.mobilecart.db.MySQLiteHelper;
import alburraq.mobilecart.dialogs.ShippingDialogFragment;
import alburraq.mobilecart.interfaces.ShippingDialogInterface;
import alburraq.mobilecart.models.Cart;
import alburraq.mobilecart.models.Shipping;
import alburraq.mobilecart.models.User;
import alburraq.mobilecart.utils.GlobalValues;
import alburraq.mobilecart.utils.PayPalConfig;

/**
 * Created by Al-Burraq Technologies on 4/17/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */
public class ShippingDetailFragment extends Fragment {

    // View with user information used to create order
    private TextInputLayout nameInputWrapper;
    private TextInputLayout streetInputWrapper;
    private TextInputLayout houseNumberInputWrapper;
    private TextInputLayout cityInputWrapper;
    private TextInputLayout zipInputWrapper;
    private TextInputLayout phoneInputWrapper;
    private TextInputLayout emailInputWrapper;
    private TextInputLayout noteInputWrapper;
    Button orderButton;
    private LinearLayout deliveryShippingLayout;
    private LinearLayout deliveryPaymentLayout;
    private TextView selectedShippingNameTv;
    private TextView selectedShippingPriceTv;
    private TextView selectedPaymentNameTv;
    private TextView selectedPaymentPriceTv;
    private Shipping selectedShipping=null;
    private LinearLayout cartItemsLayout;
    private TextView cartItemsTotalPrice;
    private TextView orderTotalPriceTv;
    TextView termsAndConditionsTv;
    MySQLiteHelper db;
    int totalPrice=0;


    User user;


    //Payment Amount
    private String paymentAmount;


    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 123;


    // Note: Your PAYPAL_CLIENT_ID should be changed in your source code before using.
    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shipping_detail_fragment, container, false);
        db=new MySQLiteHelper(getActivity());

        getUI(view);
        startPayPalService();

        clickEvents();

        return  view;

    }

    private void getUI(View view) {
        nameInputWrapper = (TextInputLayout) view.findViewById(R.id.order_create_name_wrapper);
        streetInputWrapper = (TextInputLayout) view.findViewById(R.id.order_create_street_wrapper);
        houseNumberInputWrapper = (TextInputLayout) view.findViewById(R.id.order_create_houseNumber_wrapper);
        cityInputWrapper = (TextInputLayout) view.findViewById(R.id.order_create_city_wrapper);
        zipInputWrapper = (TextInputLayout) view.findViewById(R.id.order_create_zip_wrapper);
        phoneInputWrapper = (TextInputLayout) view.findViewById(R.id.order_create_phone_wrapper);
        emailInputWrapper = (TextInputLayout) view.findViewById(R.id.order_create_email_wrapper);
        noteInputWrapper = (TextInputLayout) view.findViewById(R.id.order_create_note_wrapper);
        orderButton = (Button) view.findViewById(R.id.order_create_finish);
        selectedShippingNameTv = (TextView) view.findViewById(R.id.order_create_delivery_shipping_name);
        selectedShippingPriceTv = (TextView) view.findViewById(R.id.order_create_delivery_shipping_price);
        selectedPaymentNameTv = (TextView) view.findViewById(R.id.order_create_delivery_payment_name);
        selectedPaymentPriceTv = (TextView) view.findViewById(R.id.order_create_delivery_payment_price);
        deliveryShippingLayout =(LinearLayout) view.findViewById(R.id.order_create_delivery_shipping_layout);
        deliveryPaymentLayout =(LinearLayout) view.findViewById(R.id.order_create_delivery_payment_layout);
        cartItemsLayout = (LinearLayout) view.findViewById(R.id.order_create_cart_items_layout);
        cartItemsTotalPrice = (TextView) view.findViewById(R.id.order_create_total_price);
        orderTotalPriceTv = (TextView) view.findViewById(R.id.order_create_summary_total_price);
        termsAndConditionsTv = (TextView) view.findViewById(R.id.order_create_summary_terms_and_condition);
        termsAndConditionsTv.setText(Html.fromHtml(getString(R.string.Click_on_Order_to_allow_our_Terms_and_Conditions)));


        setUserData();
        refreshScreenContent();
    }


    void clickEvents(){
        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isRequiredFieldsOk()) {
                    if(selectedShipping!=null) {
                        paymentAmount = ""+totalPrice;
                        getPayment();
                    }else {
                        Toast.makeText(getActivity(),"Select Shipping Method First",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        deliveryShippingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShippingDialogFragment shippingDialogFragment = ShippingDialogFragment.newInstance(new ShippingDialogInterface() {
                    @Override
                    public void onShippingSelected(Shipping shipping) {
                        // Save selected value
                        selectedShipping = shipping;

                        // Update shipping related values
                        showSelectedShipping(shipping);

                        selectedPaymentNameTv.setText(getString(R.string.Choose_payment_method));
                        selectedPaymentPriceTv.setText("");
                        deliveryPaymentLayout.performClick();
                    }
                });
                shippingDialogFragment.show(getFragmentManager(), ShippingDialogFragment.class.getSimpleName());
            }
        });
    }

    private void showSelectedShipping(Shipping shipping) {
        if (shipping != null && selectedShippingNameTv != null && selectedShippingPriceTv != null) {
            selectedShippingNameTv.setText(shipping.getName());
            if (shipping.getPrice() != 0) {
                selectedShippingPriceTv.setText("$"+shipping.getPrice()+".00");
            } else {
                selectedShippingPriceTv.setText(getText(R.string.free));
            }
            totalPrice = totalPrice+shipping.getPrice();
            orderTotalPriceTv.setText("$"+totalPrice+".00");
//            deliveryPaymentLayout.setVisibility(View.VISIBLE);
        }
    }

    void startPayPalService(){
        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);
    }

    private boolean isRequiredFieldsOk() {
        // Check and show all missing values
        String fieldRequired = getString(R.string.Required_field);
        boolean nameCheck = GlobalValues.checkTextInputLayoutValueRequirement(nameInputWrapper, fieldRequired);
        boolean streetCheck = GlobalValues.checkTextInputLayoutValueRequirement(streetInputWrapper, fieldRequired);
        boolean houseNumberCheck = GlobalValues.checkTextInputLayoutValueRequirement(houseNumberInputWrapper, fieldRequired);
        boolean cityCheck = GlobalValues.checkTextInputLayoutValueRequirement(cityInputWrapper, fieldRequired);
        boolean zipCheck = GlobalValues.checkTextInputLayoutValueRequirement(zipInputWrapper, fieldRequired);
        boolean phoneCheck = GlobalValues.checkTextInputLayoutValueRequirement(phoneInputWrapper, fieldRequired);
        boolean emailCheck = GlobalValues.checkTextInputLayoutValueRequirement(emailInputWrapper, fieldRequired);

        if (nameCheck && streetCheck && houseNumberCheck && cityCheck && zipCheck && phoneCheck && emailCheck) {
            return true;
        } else {
            return false;
        }
    }


    void setUserData(){

        user=new User();
        user.setName("Ali");
        user.setCity("Lahore");
        if (user != null) {
            GlobalValues.setTextToInputLayout(nameInputWrapper, user.getName());
            GlobalValues.setTextToInputLayout(streetInputWrapper, user.getStreet());
            GlobalValues.setTextToInputLayout(houseNumberInputWrapper, user.getHouseNumber());
            GlobalValues.setTextToInputLayout(cityInputWrapper, user.getCity());
            GlobalValues.setTextToInputLayout(zipInputWrapper, user.getZip());
            GlobalValues.setTextToInputLayout(emailInputWrapper, user.getEmail());
            GlobalValues.setTextToInputLayout(phoneInputWrapper, user.getPhone());
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {
            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);
//                        Toast.makeText(getActivity(),"Process Done",Toast.LENGTH_LONG).show();
                        //Starting a new activity for the payment details and also putting the payment details with intent
                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    private void getPayment() {
        //Getting the amount
//        paymentAmount
        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(paymentAmount), "USD", "Mobile Cart",
                PayPalPayment.PAYMENT_INTENT_SALE);
        //Creating Paypal Payment activity intent
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    private void refreshScreenContent() {
        totalPrice=0;
        ArrayList<Cart> cartArrayList;
        cartArrayList=db.getCartProducts();
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < cartArrayList.size(); i++) {
            LinearLayout llRow = (LinearLayout) inflater.inflate(R.layout.order_create_cart_item, cartItemsLayout, false);
            TextView tvItemName = (TextView) llRow.findViewById(R.id.order_create_cart_item_name);
            tvItemName.setText(cartArrayList.get(i).getCART_PRODUCT_NAME());
            TextView tvItemPrice = (TextView) llRow.findViewById(R.id.order_create_cart_item_price);
            int price =cartArrayList.get(i).getCART_PRODUCT_PRICE()*cartArrayList.get(i).getCART_PRODUCT_QUANTITY();
            totalPrice=totalPrice+price;
            tvItemPrice.setText("$"+price+".00");
            TextView tvItemQuantity = (TextView) llRow.findViewById(R.id.order_create_cart_item_quantity);
            tvItemQuantity.setText(getString(R.string.format_quantity, cartArrayList.get(i).getCART_PRODUCT_QUANTITY()));
            TextView tvItemDetails = (TextView) llRow.findViewById(R.id.order_create_cart_item_details);
            tvItemDetails.setText(getString(R.string.format_string_division, cartArrayList.get(i).getCART_PRODUCT_COLOR(), cartArrayList.get(i).getCART_PRODUCT_SIZE()));
            cartItemsLayout.addView(llRow);
        }

        cartItemsTotalPrice.setText("$"+totalPrice+".00");
        orderTotalPriceTv.setText("$"+totalPrice+".00");



    }
}
