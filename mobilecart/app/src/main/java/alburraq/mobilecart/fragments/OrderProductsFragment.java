package alburraq.mobilecart.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import alburraq.mobilecart.R;
import alburraq.mobilecart.adapters.OrderProductsAdapter;
import alburraq.mobilecart.db.MySQLiteHelper;
import alburraq.mobilecart.models.Cart;

import static alburraq.mobilecart.activities.LandingActivity.editText_filter;

/**
 * Created by Al-Burraq Technologies on 4/11/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class OrderProductsFragment extends Fragment {


    /*
    *  Getting Order Data from cart and display in list
    */

    private RecyclerView mRecyclerView;
    GridLayoutManager gridLayoutManager;
    OrderProductsAdapter orderProductsAdapter;
    MySQLiteHelper db;
    Button shippingAndPaymentButton;
    Fragment fragment;
    private ArrayList<Cart> cartArrayList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.order_products_fragment, container,
                false);
        db=new MySQLiteHelper(getActivity());
        shippingAndPaymentButton = (Button) rootView.findViewById(R.id.shipping_payment_button);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.order_items_list);
        gridLayoutManager = new GridLayoutManager(getActivity(),1);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        setAdapter();


        editText_filter.setVisibility(View.GONE);
        shippingAndPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment= new ShippingDetailFragment();
                navFragmentHandler();
            }
        });
        return rootView;
    }

    void setAdapter(){
        cartArrayList=db.getCartProducts();
        orderProductsAdapter = new OrderProductsAdapter(cartArrayList,getContext());
        mRecyclerView.setAdapter(orderProductsAdapter);
    }

    void navFragmentHandler(){
        String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.addToBackStack(backStateName);
        fragmentTransaction.commit();
    }
}
