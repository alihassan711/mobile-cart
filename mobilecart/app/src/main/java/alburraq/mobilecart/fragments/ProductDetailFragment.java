package alburraq.mobilecart.fragments;

import android.content.Context;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import alburraq.mobilecart.R;
import alburraq.mobilecart.adapters.ColorSpinnerAdapter;
import alburraq.mobilecart.adapters.ProductDetailImagesAdapter;
import alburraq.mobilecart.adapters.ProductDisplayAdapter;
import alburraq.mobilecart.db.MySQLiteHelper;
import alburraq.mobilecart.interfaces.ClickListener;
import alburraq.mobilecart.models.Brand;
import alburraq.mobilecart.models.Cart;
import alburraq.mobilecart.models.Category;
import alburraq.mobilecart.models.Collection;
import alburraq.mobilecart.models.ColorList;
import alburraq.mobilecart.models.ColorsImage;
import alburraq.mobilecart.models.Price;
import alburraq.mobilecart.models.Product;
import alburraq.mobilecart.models.Size;
import alburraq.mobilecart.models.StockKeepingUnit;
import alburraq.mobilecart.utils.GlobalValues;
import alburraq.mobilecart.utils.JSONParser;

import static alburraq.mobilecart.activities.LandingActivity.badgeTv;
import static alburraq.mobilecart.activities.LandingActivity.editText_filter;
import static alburraq.mobilecart.utils.GlobalValues.selectedCategoryID;


/**
 * Created by Al-Burraq Technologies on 4/12/2017.
 * ContactNo:  +923133359533
 * Website:  http://www.al-burraq.com
 */

public class ProductDetailFragment extends Fragment implements ClickListener {

    //  Selected Product Detail
    // And Recommended Products

    int ij=0;
    MySQLiteHelper db;
    Fragment fragment;
    private RecyclerView productImagesRecycler;
    private RecyclerView recommendedProductsRecyclerView;
    LinearLayoutManager linearLayoutManager;
    LinearLayoutManager recommendedProductsLinearLayoutManager;
    ProductDetailImagesAdapter productDetailImagesAdapter;
    ProductDisplayAdapter productDisplayAdapter;
    int productID=0;
    Product product=new Product();
    ArrayList<String> images;
    int selectedColor=0;
    int selectedSize=0;
    ProgressBar product_progress;
    TextView productNameTv;
    TextView productPriceTv;
    TextView productPriceDiscountTv;
    TextView productInfoTv;
    TextView productPriceDiscountPercentTv;
    ArrayList<Product> recommendedProducts =new ArrayList<>();
    LinearLayout recommended_products_parent_layout;
    RelativeLayout left_right_arrows_layout;

    Spinner productSizeSpinner;
    Spinner productColorSpinner;

    RelativeLayout productAddToCartLayout;
    List<String> list;

    public ProductDetailFragment(){
        Log.e("Selected Product ID",""+GlobalValues.selectedProductID);
        productID=GlobalValues.selectedProductID;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.product_detail_fragment, container, false);

        db=new MySQLiteHelper(getActivity());

        product_progress = (ProgressBar) rootView.findViewById(R.id.product_progress);
        productImagesRecycler = (RecyclerView) rootView.findViewById(R.id.product_images_recycler_view);
        recommendedProductsRecyclerView = (RecyclerView) rootView.findViewById(R.id.recommended_products_recycler_view);
        productNameTv=(TextView)rootView.findViewById(R.id.product_name);
        productPriceTv=(TextView)rootView.findViewById(R.id.product_price);
        productPriceDiscountTv=(TextView)rootView.findViewById(R.id.product_price_discount);
        productInfoTv=(TextView)rootView.findViewById(R.id.product_info);
        productPriceDiscountPercentTv=(TextView)rootView.findViewById(R.id.product_price_discount_percent);
        productSizeSpinner=(Spinner)rootView.findViewById(R.id.product_size_spinner);
        productColorSpinner=(Spinner)rootView.findViewById(R.id.product_color_spinner);
        productAddToCartLayout=(RelativeLayout) rootView.findViewById(R.id.product_add_to_cart_layout);
        left_right_arrows_layout=(RelativeLayout) rootView.findViewById(R.id.left_right_arrows_layout);
        recommended_products_parent_layout=(LinearLayout) rootView.findViewById(R.id.recommended_products_parent_layout);



        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recommendedProductsLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        productImagesRecycler.setLayoutManager(linearLayoutManager);
        recommendedProductsRecyclerView.setLayoutManager(recommendedProductsLinearLayoutManager);
        left_right_arrows_layout.setVisibility(View.GONE);

        setAdapter();
        setProductData();

        setSizeSpinner();
        setColorSpinner();

        badgeTv.setText(""+db.getCartAllProductsCount());
        if(db.getCartAllProductsCount()>0){
            badgeTv.setVisibility(View.VISIBLE);
        }else {
            badgeTv.setVisibility(View.GONE);
        }

        editText_filter.setVisibility(View.GONE);


        productAddToCartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cart cart=new Cart();
                cart.setCART_PRODUCT_ID(product.getPRODUCT_ID());
                cart.setCART_PRODUCT_COLOR(product.getCOLORS().get(selectedColor).getNAME());
                cart.setCART_PRODUCT_SIZE(list.get(selectedSize));
                cart.setCART_PRODUCT_IMAGE(images.get(0));
                cart.setCART_PRODUCT_NAME(product.getPRODUCT_NAME());
                cart.setCART_PRODUCT_PRICE(product.getPRICE().getPROMO_PRICE());
                cart.setCART_PRODUCT_QUANTITY(1);

                db.addToCart(cart);
                badgeTv.setText(""+db.getCartAllProductsCount());
                if(db.getCartAllProductsCount()>0){
                    badgeTv.setVisibility(View.VISIBLE);
                }else {
                    badgeTv.setVisibility(View.GONE);
                }


            }
        });
        return rootView;
    }

    void setAdapter(){

//        Getting selected product detail based on selected product ID
        getProduct();
        productDetailImagesAdapter = new ProductDetailImagesAdapter(images,getContext());
        productImagesRecycler.setAdapter(productDetailImagesAdapter);
        product_progress.setVisibility(View.GONE);
        setRecommendedProductsAdapter();
    }

    void setRecommendedProductsAdapter(){
//        for (int i=0;i<GlobalValues.recommendedProducts.size();i++){
//            List<Category> categoryList=new ArrayList<>();
//            categoryList=GlobalValues.recommendedProducts.get(i).getCATEGORIES();
//            for (int j=0;j<categoryList.size();j++){
//                if(selectedCategoryID ==categoryList.get(j).getCATEGORYID()){
//                    recommendedProducts.add(GlobalValues.recommendedProducts.get(i));
//                }
//            }
//        }

        recommendedProducts =new ArrayList<>();

        getRecommendedProducts(product.getPRODUCT_ID());

    }

    void getProduct(){
        for (int i=0;i< GlobalValues.products.size();i++){
            if(GlobalValues.products.get(i).getPRODUCT_ID()==productID){
                product=GlobalValues.products.get(i);
//                Log.e("Product ID",""+product.getPRODUCT_ID());
                break;
            }
        }
        getImages();
    }

    void getImages(){
        images=new ArrayList<>();
        List<ColorList> colorList =product.getCOLORS();
        List<ColorsImage> colorsImageList=colorList.get(selectedColor).getIMAGES();

        for (int j=0;j<colorsImageList.size();j++){
            String imageURL = colorsImageList.get(j).getTHUMBNAIL();
            images.add(imageURL);
        }

        if(images.size()>1){
//            left_right_arrows_layout.setVisibility(View.VISIBLE);
            left_right_arrows_layout.setVisibility(View.GONE);
        }else {
            left_right_arrows_layout.setVisibility(View.GONE);
        }
    }

    void setProductData(){
        productNameTv.setText(product.getPRODUCT_NAME());

        // Determine if product is on sale
        double pr = product.getPRICE().getPRICE();
        double dis = product.getPRICE().getPROMO_PRICE();
        if (pr == dis || Math.abs(pr - dis) / Math.max(Math.abs(pr), Math.abs(dis)) < 0.000001) {
            productPriceDiscountTv.setText("$"+product.getPRICE().getPRICE()+".00");
            productPriceDiscountTv.setTextColor(ContextCompat.getColor(getContext(), R.color.textPrimary));
            productPriceTv.setVisibility(View.GONE);
            productPriceDiscountPercentTv.setVisibility(View.GONE);
        } else {
            productPriceDiscountTv.setText("$"+product.getPRICE().getPROMO_PRICE()+".00");
            productPriceDiscountTv.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
            productPriceTv.setVisibility(View.VISIBLE);
            productPriceTv.setText("$"+product.getPRICE().getPRICE()+".00");
            productPriceTv.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
            productPriceDiscountPercentTv.setVisibility(View.VISIBLE);
            productPriceDiscountPercentTv.setText(calculateDiscountPercent(getContext(), pr, dis));
        }
        if (product.getDESCRIPTION() != null) {
            productInfoTv.setText(product.getDESCRIPTION());
        }
    }


     void setSizeSpinner(){

         list=new ArrayList<String>();

         for (int i=0;i<product.getSIZES().size();i++){
             list.add(""+product.getSIZES().get(i).getCODE());
         }

         ArrayAdapter<String> adp= new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,list);
         adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         productSizeSpinner.setAdapter(adp);
         productSizeSpinner.setSelection(selectedSize);

         productSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

             @Override
             public void onItemSelected(AdapterView<?> arg0, View arg1,int pos, long arg3) {
                 selectedSize=pos;
//                 Toast.makeText(getContext(),productSizeSpinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
             }
             @Override
             public void onNothingSelected(AdapterView<?> arg0) {
                 // TODO Auto-generated method stub
             }
         });
     }

     void setColorSpinner(){

         List<ColorList> productColors = new ArrayList<>();
         productColors=product.getCOLORS();
         if (productColors.size() > 1) {
             productColorSpinner.setVisibility(View.VISIBLE);
             ColorSpinnerAdapter colorSpinnerAdapter = new ColorSpinnerAdapter(getActivity());
             colorSpinnerAdapter.setProductColorList(productColors);
             productColorSpinner.setAdapter(colorSpinnerAdapter);
             productColorSpinner.setSelection(selectedColor);
             productColorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                 @Override
                 public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                     ColorList productColor = (ColorList) parent.getItemAtPosition(position);
                     selectedColor=position;
                     setAdapter();
                 }

                 @Override
                 public void onNothingSelected(AdapterView<?> parent) {
                 }
             });
         }else {
             productColorSpinner.setVisibility(View.GONE);
         }
     }

    String calculateDiscountPercent(Context context, double price, double discountPrice) {
        int percent;
        if (discountPrice >= price) {
            percent = 0;
        } else {
            percent = (int) Math.round(100 - ((discountPrice / price) * 100));
        }
        return String.format(context.getString(R.string.format_price_discount_percents), percent);
    }

    @Override
    public void itemClicked(View view, int position, ArrayList<Product> productArrayList) {
        GlobalValues.selectedProductID=productArrayList.get(position).getPRODUCT_ID();
        fragment= new ProductDetailFragment();
        navFragmentHandler();
    }

    void navFragmentHandler(){
        String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.addToBackStack(backStateName);
        fragmentTransaction.commit();
    }

    public void getRecommendedProducts(final int pID) {

        new AsyncTask<String, Void, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected JSONObject doInBackground(String... strings) {
                JSONParser parser = new JSONParser();
                JSONObject JObject = parser.makeHttpRequest(GlobalValues.BASE_URL + "getRecommendedProducts/"+pID, "GET", null);
                return JObject;
            }

            @Override
            protected void onPostExecute(JSONObject JObject) {
                String status = null;
                if (JObject != null) {
                    try {
                        status = JObject.get("success").toString();
                        if(status.equals("0")){
                            String msg=JObject.get("message").toString();
//                            Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();
                            return;
                        }
                        else if(status.equals("1")){
                            if(JObject.has("products")) {
                                JSONArray productsJsonArray = JObject.getJSONArray("products");
                                for(int i=0;i<productsJsonArray.length();i++) {

                                    JSONObject jsonObj=productsJsonArray.getJSONObject(i);
                                    Product product = new Product();

                                    product.setPRODUCT_NAME(jsonObj.getString("productName" ));
                                    product.setBEST_SALES_RANK(jsonObj.getInt("bestSaleRank"));
                                    product.setPRODUCT_ID(jsonObj.getInt("productId"));
                                    product.setITEM_GROUP_ID(jsonObj.getInt("itemGroupId"));
                                    int isNewInt=jsonObj.getInt("isNew");
                                    int saleInt=jsonObj.getInt("sale");
                                    if(isNewInt==0){
                                        product.setIS_NEW(false);
                                    }else{ product.setIS_NEW(true); }

                                    if(saleInt==0){
                                        product.setSALE(false);
                                    }else { product.setSALE(true); }


                                    Brand brand = new Brand();
                                    JSONObject brandJsonObject = jsonObj.getJSONObject("brand");
                                    brand.setBRAND_ID(brandJsonObject.getInt("brandId"));
                                    brand.setBRAND_NAME(brandJsonObject.getString("brandName"));
                                    brand.setBRAND_IMG(brandJsonObject.getString("brandImage"));
                                    int isPremium=brandJsonObject.getInt("isPremium");
                                    if(isPremium==0){
                                        brand.setIS_PREMIUM(false);
                                    }else {
                                        brand.setIS_PREMIUM(true);
                                    }

                                    product.setBRAND(brand);

                                    product.setDESCRIPTION(jsonObj.getString("description"));
                                    product.setPRODUCT_URL(jsonObj.getString("productUrl"));

                                    Price price = new Price();
//                                    JSONObject priceJsonObject = jsonObj.getJSONObject("PRICE");
                                    price.setPRICE(jsonObj.getInt("price"));
                                    price.setPROMO_PRICE(jsonObj.getInt("promoPrice"));
                                    product.setPRICE(price);

                                    List<Size> sizeList=new ArrayList<>();
                                    JSONArray sizesJsonArray = jsonObj.getJSONArray("sizes");
                                    for (int s=0;s<sizesJsonArray.length();s++){
                                        JSONObject sizeJsonObject=sizesJsonArray.getJSONObject(s);
                                        Size size=new Size();
                                        size.setID(sizeJsonObject.getInt("sizeId"));
                                        size.setCODE(sizeJsonObject.getInt("sizeCode"));
                                        size.setNAME(sizeJsonObject.getString("sizeName"));
                                        sizeList.add(size);
                                    }
                                    product.setSIZES(sizeList);

                                    List<ColorList> colorList=new ArrayList<>();
                                    JSONArray colorsJsonArray = jsonObj.getJSONArray("colors");
                                    for (int c=0;c<colorsJsonArray.length();c++){
                                        JSONObject colorsJsonObject=colorsJsonArray.getJSONObject(c);
                                        ColorList color=new ColorList();
                                        color.setID(colorsJsonObject.getInt("colorId"));
                                        color.setNAME(colorsJsonObject.getString("colorName"));
                                        color.setCODE(colorsJsonObject.getString("code"));
                                        color.setRGB(colorsJsonObject.getString("rgb"));

                                        List<ColorsImage> colorsImageList=new ArrayList<>();
                                        JSONArray colorsImageJsonArray = colorsJsonObject.getJSONArray("colorImages");
                                        for (int ci=0;ci<colorsImageJsonArray.length();ci++){
                                            JSONObject colorImageJsonObject=colorsImageJsonArray.getJSONObject(ci);
                                            ColorsImage colorsImage= new ColorsImage();
                                            colorsImage.setIMAGE(colorImageJsonObject.getString("image"));
                                            colorsImage.setTHUMBNAIL(colorImageJsonObject.getString("thumbnail"));
                                            colorsImageList.add(colorsImage);
                                        }
                                        color.setIMAGES(colorsImageList);

                                        colorList.add(color);
                                    }
                                    product.setCOLORS(colorList);

                                    List<Collection> collectionList=new ArrayList<>();
                                    JSONArray collectionJsonArray=jsonObj.getJSONArray("collections");
                                    for (int col=0;col<collectionJsonArray.length();col++){
                                        JSONObject collectionJsonObject=collectionJsonArray.getJSONObject(col);
                                        Collection collection=new Collection();
                                        collection.setCOLLECTION_ID(collectionJsonObject.getInt("collectionId"));
                                        collection.setCOLLECTIONTEXT(collectionJsonObject.getString("collectionText"));
                                        collectionList.add(collection);
                                    }
                                    product.setCOLLECTIONS(collectionList);

                                    List<Category> categoryList=new ArrayList<>();
                                    JSONArray categoriesJsonArray=jsonObj.getJSONArray("categories");
                                    for (int cat=0;cat<categoriesJsonArray.length();cat++){
                                        JSONObject categoryJsonObject=categoriesJsonArray.getJSONObject(cat);
                                        Category category=new Category();
                                        category.setCATEGORYID(categoryJsonObject.getInt("categoryId"));
                                        category.setCATEGORYTEXT(categoryJsonObject.getString("categoryText"));
                                        categoryList.add(category);
                                    }
                                    product.setCATEGORIES(categoryList);

                                    List<StockKeepingUnit> stockKeepingUnitList=new ArrayList<>();
                                    JSONArray skuJsonArray=jsonObj.getJSONArray("skus");
                                    for (int sk=0;sk<skuJsonArray.length();sk++){
                                        JSONObject skuJsonObject=skuJsonArray.getJSONObject(sk);
                                        StockKeepingUnit stockKeepingUnit=new StockKeepingUnit();
                                        stockKeepingUnit.setID(skuJsonObject.getInt("skuId"));
                                        stockKeepingUnit.setPCS_IN_STOCK(skuJsonObject.getInt("pcsInStock"));
                                        stockKeepingUnit.setCOLOR_ID(skuJsonObject.getInt("colorId"));
                                        stockKeepingUnit.setSIZE_ID(skuJsonObject.getInt("sizeId"));

                                        stockKeepingUnitList.add(stockKeepingUnit);
                                    }
                                    product.setSKUS(stockKeepingUnitList);

                                    boolean isFound=true;
                                    // add single product data in product arrayList for later use
                                    for(int g=0;g<GlobalValues.products.size();g++){
                                        if(GlobalValues.products.get(g).getPRODUCT_ID()==product.getPRODUCT_ID()){
                                            isFound=false;
                                        }
                                    }

                                    if(!isFound) {
                                        GlobalValues.products.add(product);
                                    }

                                    recommendedProducts.add(product);
                                }

                                if(recommendedProducts.size()==0){
                                    recommended_products_parent_layout.setVisibility(View.GONE);
                                }

                                productDisplayAdapter = new ProductDisplayAdapter(recommendedProducts,getContext());
                                recommendedProductsRecyclerView.setAdapter(productDisplayAdapter);
                                productDisplayAdapter .setClickListener(ProductDetailFragment.this);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }
}
