package alburraq.mobilecart.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import alburraq.mobilecart.R;
import alburraq.mobilecart.adapters.ProductDisplayAdapter;
import alburraq.mobilecart.db.MySQLiteHelper;
import alburraq.mobilecart.interfaces.ClickListener;
import alburraq.mobilecart.models.Brand;
import alburraq.mobilecart.models.Category;
import alburraq.mobilecart.models.Collection;
import alburraq.mobilecart.models.ColorList;
import alburraq.mobilecart.models.ColorsImage;
import alburraq.mobilecart.models.Price;
import alburraq.mobilecart.models.Product;
import alburraq.mobilecart.models.Size;
import alburraq.mobilecart.models.StockKeepingUnit;
import alburraq.mobilecart.utils.EndlessScrollListener;
import alburraq.mobilecart.utils.GlobalValues;
import alburraq.mobilecart.utils.JSONParser;

import static alburraq.mobilecart.activities.LandingActivity.badgeTv;
import static alburraq.mobilecart.activities.LandingActivity.editText_filter;

/**
 * Created by Al-Burraq-Dev on 5/25/2017.
 */

public class ListProductsByCategory extends Fragment implements ClickListener {

    /*
    * Get Products data according to selected category form navigation drawer and display in list
    */


    RecyclerView mRecyclerView;
    private GridLayoutManager mLayoutManager;
    LinearLayout empty_list;
    ProductDisplayAdapter adapter;
    ArrayList<Product> products=new ArrayList<>();
    Fragment fragment;
    MySQLiteHelper db;
    private TextWatcher mSearchTw;

    // Store a member variable for the listener
    private EndlessScrollListener scrollListener;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_landing_page, container, false);

        db=new MySQLiteHelper(getActivity());


        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list_items);
        empty_list=(LinearLayout)rootView.findViewById(R.id.empty_list);

        mLayoutManager = new GridLayoutManager(getContext(),2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);

        setAdapter();


        badgeTv.setText(""+db.getCartAllProductsCount());
        if(db.getCartAllProductsCount()>0){
            badgeTv.setVisibility(View.VISIBLE);
        }else {
            badgeTv.setVisibility(View.GONE);
        }


        mSearchTw=new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        editText_filter.setVisibility(View.VISIBLE);
        editText_filter.addTextChangedListener(mSearchTw);

        return rootView;
    }

    public void setAdapter(){


        // Getting data by selected Category ID

        products=new ArrayList<Product>();
        GlobalValues.products=new ArrayList<Product>();
        getProducts(0,true);

        // Retain an instance so that you can call `resetState()` for fresh searches
        scrollListener = new EndlessScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.e("Page",""+page);
                getProducts(page,false);
            }
        };
        // Adds the scroll listener to RecyclerView
        mRecyclerView.addOnScrollListener(scrollListener);
    }

    @Override
    public void itemClicked(View view, int position, ArrayList<Product> productsArrayList) {
        GlobalValues.selectedProductID=productsArrayList.get(position).getPRODUCT_ID();
        fragment= new ProductDetailFragment();
        navFragmentHandler();
    }

    void navFragmentHandler(){
        String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.addToBackStack(backStateName);
        fragmentTransaction.commit();
    }

    public void getProducts(final int offSet, final boolean isStart) {

        new AsyncTask<String, Void, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected JSONObject doInBackground(String... strings) {
                JSONParser parser = new JSONParser();
                JSONObject JObject = parser.makeHttpRequest(GlobalValues.BASE_URL + "getProducts/"+offSet+"/4", "GET", null);
                return JObject;
            }

            @Override
            protected void onPostExecute(JSONObject JObject) {
                String status = null;
                if (JObject != null) {
                    try {
                        status = JObject.get("success").toString();
                        if(status.equals("0")){
                            String msg=JObject.get("message").toString();
//                            Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();
                            return;
                        }
                        else if(status.equals("1")){
                            if(JObject.has("products")) {
                                JSONArray productsJsonArray = JObject.getJSONArray("products");
                                for(int i=0;i<productsJsonArray.length();i++) {

                                    JSONObject jsonObj=productsJsonArray.getJSONObject(i);
                                    Product product = new Product();

                                    product.setPRODUCT_NAME(jsonObj.getString("productName" ));
                                    product.setBEST_SALES_RANK(jsonObj.getInt("bestSaleRank"));
                                    product.setPRODUCT_ID(jsonObj.getInt("productId"));
                                    product.setITEM_GROUP_ID(jsonObj.getInt("itemGroupId"));
                                    int isNewInt=jsonObj.getInt("isNew");
                                    int saleInt=jsonObj.getInt("sale");
                                    if(isNewInt==0){
                                        product.setIS_NEW(false);
                                    }else{ product.setIS_NEW(true); }

                                    if(saleInt==0){
                                        product.setSALE(false);
                                    }else { product.setSALE(true); }


                                    Brand brand = new Brand();
                                    JSONObject brandJsonObject = jsonObj.getJSONObject("brand");
                                    brand.setBRAND_ID(brandJsonObject.getInt("brandId"));
                                    brand.setBRAND_NAME(brandJsonObject.getString("brandName"));
                                    brand.setBRAND_IMG(brandJsonObject.getString("brandImage"));
                                    int isPremium=brandJsonObject.getInt("isPremium");
                                    if(isPremium==0){
                                        brand.setIS_PREMIUM(false);
                                    }else {
                                        brand.setIS_PREMIUM(true);
                                    }

                                    product.setBRAND(brand);

                                    product.setDESCRIPTION(jsonObj.getString("description"));
                                    product.setPRODUCT_URL(jsonObj.getString("productUrl"));

                                    Price price = new Price();
//                                    JSONObject priceJsonObject = jsonObj.getJSONObject("PRICE");
                                    price.setPRICE(jsonObj.getInt("price"));
                                    price.setPROMO_PRICE(jsonObj.getInt("promoPrice"));
                                    product.setPRICE(price);

                                    List<Size> sizeList=new ArrayList<>();
                                    JSONArray sizesJsonArray = jsonObj.getJSONArray("sizes");
                                    for (int s=0;s<sizesJsonArray.length();s++){
                                        JSONObject sizeJsonObject=sizesJsonArray.getJSONObject(s);
                                        Size size=new Size();
                                        size.setID(sizeJsonObject.getInt("sizeId"));
                                        size.setCODE(sizeJsonObject.getInt("sizeCode"));
                                        size.setNAME(sizeJsonObject.getString("sizeName"));
                                        sizeList.add(size);
                                    }
                                    product.setSIZES(sizeList);

                                    List<ColorList> colorList=new ArrayList<>();
                                    JSONArray colorsJsonArray = jsonObj.getJSONArray("colors");
                                    for (int c=0;c<colorsJsonArray.length();c++){
                                        JSONObject colorsJsonObject=colorsJsonArray.getJSONObject(c);
                                        ColorList color=new ColorList();
                                        color.setID(colorsJsonObject.getInt("colorId"));
                                        color.setNAME(colorsJsonObject.getString("colorName"));
                                        color.setCODE(colorsJsonObject.getString("code"));
                                        color.setRGB(colorsJsonObject.getString("rgb"));

                                        List<ColorsImage> colorsImageList=new ArrayList<>();
                                        JSONArray colorsImageJsonArray = colorsJsonObject.getJSONArray("colorImages");
                                        for (int ci=0;ci<colorsImageJsonArray.length();ci++){
                                            JSONObject colorImageJsonObject=colorsImageJsonArray.getJSONObject(ci);
                                            ColorsImage colorsImage= new ColorsImage();
                                            colorsImage.setIMAGE(colorImageJsonObject.getString("image"));
                                            colorsImage.setTHUMBNAIL(colorImageJsonObject.getString("thumbnail"));
                                            colorsImageList.add(colorsImage);
                                        }
                                        color.setIMAGES(colorsImageList);

                                        colorList.add(color);
                                    }
                                    product.setCOLORS(colorList);

                                    List<Collection> collectionList=new ArrayList<>();
                                    JSONArray collectionJsonArray=jsonObj.getJSONArray("collections");
                                    for (int col=0;col<collectionJsonArray.length();col++){
                                        JSONObject collectionJsonObject=collectionJsonArray.getJSONObject(col);
                                        Collection collection=new Collection();
                                        collection.setCOLLECTION_ID(collectionJsonObject.getInt("collectionId"));
                                        collection.setCOLLECTIONTEXT(collectionJsonObject.getString("collectionText"));
                                        collectionList.add(collection);
                                    }
                                    product.setCOLLECTIONS(collectionList);

                                    List<Category> categoryList=new ArrayList<>();
                                    JSONArray categoriesJsonArray=jsonObj.getJSONArray("categories");
                                    for (int cat=0;cat<categoriesJsonArray.length();cat++){
                                        JSONObject categoryJsonObject=categoriesJsonArray.getJSONObject(cat);
                                        Category category=new Category();
                                        category.setCATEGORYID(categoryJsonObject.getInt("categoryId"));
                                        category.setCATEGORYTEXT(categoryJsonObject.getString("categoryText"));
                                        categoryList.add(category);
                                    }
                                    product.setCATEGORIES(categoryList);

                                    List<StockKeepingUnit> stockKeepingUnitList=new ArrayList<>();
                                    JSONArray skuJsonArray=jsonObj.getJSONArray("skus");
                                    for (int sk=0;sk<skuJsonArray.length();sk++){
                                        JSONObject skuJsonObject=skuJsonArray.getJSONObject(sk);
                                        StockKeepingUnit stockKeepingUnit=new StockKeepingUnit();
                                        stockKeepingUnit.setID(skuJsonObject.getInt("skuId"));
                                        stockKeepingUnit.setPCS_IN_STOCK(skuJsonObject.getInt("pcsInStock"));
                                        stockKeepingUnit.setCOLOR_ID(skuJsonObject.getInt("colorId"));
                                        stockKeepingUnit.setSIZE_ID(skuJsonObject.getInt("sizeId"));

                                        stockKeepingUnitList.add(stockKeepingUnit);
                                    }
                                    product.setSKUS(stockKeepingUnitList);

                                    // add single product data in product arrayList for later use
                                    GlobalValues.products.add(product);
                                    products.add(product);
                                }

                                if(isStart) {
                                    if (products.size() == 0) {
                                        empty_list.setVisibility(View.VISIBLE);
                                    }

                                    adapter = new ProductDisplayAdapter(products, getContext());
                                    mRecyclerView.setAdapter(adapter);
                                    adapter.setClickListener(ListProductsByCategory.this);
//                                hcjgkhghfdgj
                                }else {
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }
}
