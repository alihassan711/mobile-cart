**[Documentation Mobile Commerce](https://docs.google.com/document/d/1Y2xms-jWRBH3WnYAx41JjqX5gt7uNdKREsB5g_sq5Ms/edit?usp=sharing)**

How You can Create Your Own E-Commerce Mobile Application for Free?
Solution provided by Al-Burraq Technologies

**OVERVIEW & PURPOSE**
Mobile Usage is increasing day by day. Laptops replaced Desktops in Past and nowadays Mobile Phones are replacing Laptops. And in fact facilitating more than Laptops ever did. Considering that all Stores Moved online initially and now these are moving toward Mobile applications. We have developed an Open Source Solution for those online stores who do not have either Mobile apps or they need to improve apps in Low Cost. Our Solution is free of Cost and It is uploaded along with the documentation i-e How to build it your own using minimal Coding. 

**Functional Aspects**
Login with Facebook, Twitter or direct Login and Register.
This app handles Categories and SubCategories via Multi Level Navigation Drawer.
List of products is displayed according to the orientation of the device. If portrait it will show you 1 or 2 products otherwise more in case of Landscape.

You can Search between All the products, searching is implemented using multiple filters.
Multiple Size/Types are handled in efficient way, adding more than one size will add 2 different items in Cart.
Multiple images of product is also handles in a very fancy way.
Shipping Methods can also managed in this application.
Cart is implemented with full features considering a general Store. 
Checkout when you are good to go and further proceed your order.
Order screen order short summary, Shipping method selection, Shipping Detail.
Paypal integrated with the solution, That means you are good to go and implement it for your online store.
Clean Design is implemented as Max as Possible, still we are open for the suggestions and your feedback.

**OBJECTIVES**
Provide an Open Source Mobile Solution for IOS as well and make it a complete portal.
Meet User’s Need.
Give back to community.

**MATERIALS NEEDED**
To run this solution You need Keys for twitter/FB/Paypal Merchant Account.
To Connect it with your current systems You need to develop APIs according to the documentation and connect them at the right place. You can see the comments in the code as well as documentation will help you out. 
If you need any kind of help you can email us for the appointment at info@al-burraq.com.

**Development Documentation**
Login with Facebook, Twitter or direct Login and Register.
Create Application on Facebook developer console get Facebook API key replace it test key.
Install fabric on android studio, register App on Twitter with TWITTER_KEY and TWITTER_SECRET keys replace with test keys.
For Direct login register user first save data on server.
Navigation Drawer shows items Category and sub-category.
Get Items category and sub-category data from server or enter manually into menu JSON file inside raw folder. Define category and sub-category properly for reference check menu JSON file inside raw folder.
Get Data from JSON and add into menu arraylist which sort category and sub-category accordingly and display in Navigation drawer.
Main Fragment show data according to user selection from category and sub-category.
Get products from server or enter manually into product JSON file.
Get data from JSON and add into product arraylist.
When you select item from navigation drawer it will automatically pick items based on category and sub-category ID.
 
**Search product**
Search is implemented no need to change search with product name or description.
Search can be improve more depends on requirements.
Click on Item open item detail screen with different images, detailed price, different sizes and different colors with item description.

This screen get data from product array list.
Get data based on product Id passed on single item click.
Recommended items list.
Get recommended items from server or you can also get recommended item based on item you watched.
Save recommended items into array list of products type.
Add item to cart from item detail screen.
Cart screen display items list you added in cart.
Add items into cart save in database.
Get Carts items from database and display.
Edit item from cart increase or decrease size, change item color or delete item from cart.
Delete Item from cart and also from database.
Edit Item from cart. 
Order screen order short summary, Shipping method selection, Shipping Detail.
Get shipping methods list from Server or add into shipping JSON file and save methods into arraylist.
Get methods from arraylist display and select shipping method.
Place order pay amount via PayPal.
Register on developer PayPal console, create application.
Get PAYPAL_CLIENT_ID replace it with test id and get payment on your account.